package cn.org.theo.mapper;

import cn.org.theo.pojo.Manager;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.*;

/**
 * project name: IntelliJ IDEA
 * Date: 2022/8/23 21:35
 *
 * @PackageName: cn.org.theo.mapper
 * @ClassName: ManagerMapperTest
 * @Author: Theo
 * @Version:
 * @Description:
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:applicationContext.xml"})
public class ManagerMapperTest {

    @Autowired
    private ManagerMapper managerMapper;

    @Test
    public void a(){
        Manager manager = new Manager();
        manager.setUsername("admin");
        manager.setPassword("admin");
        Manager query = managerMapper.query(manager);
        System.out.println(query);
    }

    @Test
    public void getMaxNum() {
        System.out.println(managerMapper.getMaxNum());
    }

    @Test
    public void insert() {
        Manager manager = new Manager();
        manager.setRole(2);
        manager.setStatus(1);
        manager.setUsername("67890");
        manager.setPassword("654321");
        managerMapper.insert(manager);
        System.out.println(manager);
    }
}