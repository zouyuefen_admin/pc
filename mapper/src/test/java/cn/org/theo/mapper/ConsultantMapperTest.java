package cn.org.theo.mapper;

import cn.org.theo.pojo.Consultant;
import cn.org.theo.vo.ConsultantVo;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.*;

import static org.junit.Assert.*;

/**
 * project name: IntelliJ IDEA
 * Date: 2022/8/24 21:14
 *
 * @PackageName: cn.org.theo.mapper
 * @ClassName: ConsultantMapperTest
 * @Author: Theo
 * @Version:
 * @Description:
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:applicationContext.xml"})
public class ConsultantMapperTest {
    @Autowired
    private ConsultantMapper consultantMapper;
    @Test
    public void queryPositionalTitles() {
        List<Consultant> consultants =
                consultantMapper.queryPositionalTitles();

        Set<Consultant> set = new HashSet<>();
        consultants.forEach(t->set.add(t));
        set.forEach(System.out::println);

    }

    @Test
    public void queryForConditionByPage() {
        Map<String,Object> map = new HashMap<>();
        map.put("name","%王%");
        map.put("positionalTitles","硕士");
        map.put("limit",0);
        map.put("offset",5);
        map.put("status",1);
        List<ConsultantVo> consultants = consultantMapper.queryForConditionByPage(map);
        consultants.forEach(System.out::println);
    }

    @Test
    public void queryTotalCount() {

        int i = consultantMapper.queryTotalCount(new ConsultantVo());
        System.out.println(i);

    }

    @Test
    public void query() {
//        ConsultantVo consultant = new ConsultantVo();
//        consultant.setId(12);
//        Consultant query = consultantMapper.query(consultant);
//        System.out.println(query);
    }

    @Test
    public void query2() {


    }
}