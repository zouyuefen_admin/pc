package cn.org.theo.mapper;

import org.junit.Test;

import java.text.SimpleDateFormat;
import java.util.Date;

import static org.junit.Assert.*;

/**
 * project name: IntelliJ IDEA
 * Date: 2022/8/26 11:55
 *
 * @PackageName: cn.org.theo.mapper
 * @ClassName: OrderStatusMapperTest
 * @Author: Theo
 * @Version:
 * @Description:
 */
public class OrderStatusMapperTest {

    @Test
    public void query() {
        Date date = new Date(1661524900000L);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String format = simpleDateFormat.format(date);
        System.out.println(format);
    }
}