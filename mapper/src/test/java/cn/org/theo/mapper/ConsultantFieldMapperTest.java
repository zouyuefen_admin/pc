package cn.org.theo.mapper;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

/**
 * project name: IntelliJ IDEA
 * Date: 2022/8/28 13:25
 *
 * @PackageName: cn.org.theo.mapper
 * @ClassName: ConsultantFieldMapperTest
 * @Author: Theo
 * @Version:
 * @Description:
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:applicationContext.xml"})
public class ConsultantFieldMapperTest {


    @Autowired
    private ConsultantFieldMapper consultantFieldMapper;

    @Test
    public void queryId() {
        List<Integer> list = new ArrayList<>();
        list.add(2);
        list.add(3);
        consultantFieldMapper.queryId(list).forEach(System.out::println);

    }
}