package cn.org.theo.mapper;

import cn.org.theo.pojo.Assessment;
import cn.org.theo.pojo.AssessmentVo;
import cn.org.theo.qo.AssessmentQo;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.HashMap;
import java.util.List;

import static org.junit.Assert.*;

/**
 * project name: IntelliJ IDEA
 * Date: 2022/9/1 16:40
 *
 * @PackageName: cn.org.theo.mapper
 * @ClassName: AssessmentMapperTest
 * @Author: Theo
 * @Version:
 * @Description:
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:applicationContext.xml"})
public class AssessmentMapperTest {

    @Autowired
    private AssessmentMapper assessmentMapper;
    @Test
    public void queryForConditionByPage() {
        HashMap<String,Object> map = new HashMap<>();
        AssessmentQo assessmentQo = new AssessmentQo();
//        assessmentQo.setScore1(80);
//        assessmentQo.setScore2(90);
        map.put("score1",assessmentQo.getScore1());
        map.put("score2",assessmentQo.getScore2());
        map.put("date1",assessmentQo.getDate1());
        map.put("date2",assessmentQo.getDate2());
        map.put("limit",0);
        map.put("offset",5);
        List<AssessmentVo> assessments =
                assessmentMapper.queryForConditionByPage(map);
        assessments.forEach(System.out::println);
        int i = assessmentMapper.queryTotalCount(map);
        System.out.println(i);
    }

    @Test
    public void queryTotalCount() {
    }
}