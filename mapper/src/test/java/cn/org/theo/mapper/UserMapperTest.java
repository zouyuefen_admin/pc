package cn.org.theo.mapper;

import cn.org.theo.pojo.Page;
import cn.org.theo.pojo.User;
import cn.org.theo.qo.Week;
import cn.org.theo.vo.WeekData;
import org.apache.ibatis.logging.stdout.StdOutImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;

/**
 * project name: IntelliJ IDEA
 * Date: 2022/8/23 21:34
 *
 * @PackageName: cn.org.theo.mapper
 * @ClassName: UserMapperTest
 * @Author: Theo
 * @Version:
 * @Description:
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:applicationContext.xml"})
public class UserMapperTest {
    @Autowired
    private UserMapper userMapper;

    @Test
    public void addUser(){
        User user = new User();
        user.setPassword("112211212");
        user.setUsername("Theohj");
        user.setSex("男");
        user.setAge(22);
        user.setPhone("112211212");
        user.setAddress("泉州");
        int i = userMapper.addUser(user);
        System.out.println(i);
//        String property = System.getProperty("user.dir");
//        System.out.println(property);
    }
    @Test
    public void query(){
        User user = new User();
//        user.setPassword("112211212");
        user.setUsername("Theo");
        System.out.println(user);
//        user.setSex("男");
//        user.setAge(22);
//        user.setPhone("112211212");
//        user.setAddress("泉州");
        User i = userMapper.query(user);
        System.out.println(i);
    }
    @Test
    public void query2(){
        Page<User> page = new Page<>();
        page.setLimit(0);
        page.setOffset(5);

        List<User> users = userMapper.queryByPage(page);
        users.forEach(System.out::println);
    }
    @Test
    public void query3(){
        System.out.println(userMapper.queryTotalCount(new User()));
    }
    @Test
    public void update(){
        User user = new User();
        user.setId(13);
        user.setStatus(3);
//        System.out.println(userMapper.update(user));
        System.out.println(userMapper.queryTotalCount(user));
    }
    @Test
    public void del(){

        System.out.println(userMapper.delete(13));
    }

    @Test
    public void queryForConditionByPage() {
        Map<String,Object> map = new HashMap<>();
        map.put("username","%hx%");
        map.put("status",1);
        map.put("limit",4);
        map.put("offset",5);
        List<User> users = userMapper.queryForConditionByPage(map);
        users.forEach(System.out::println);
    }

    @Test
    public void testQuery() {
        User user = new User();
        user.setUsername("Theo666");
        System.out.println(userMapper.query(user));
    }

    @Test
    public void costMoney() {

        HashMap<String,Object> map = new HashMap<>();
        map.put("servicePrice",200);
        map.put("id",1);
        int i2  = userMapper.costMoney(map);

    }

    @Test
    public void queryForWeekData() {
        Week week = new Week();
        week.setW1("2022-09-05");
        week.setW2("2022-09-06");
        week.setW3("2022-09-07");
        week.setW4("2022-09-08");
        week.setW5("2022-09-09");
        week.setW6("2022-09-10");
        week.setW7("2022-09-11");
        List<String> list = new ArrayList<>();
        list.add(week.getW1());
        list.add(week.getW2());
        list.add(week.getW3());
        list.add(week.getW4());
        list.add(week.getW5());
        list.add(week.getW6());
        list.add(week.getW7());
        List<WeekData> weekDataList = userMapper.queryForWeekData(list);
        weekDataList.forEach(System.out::println);
    }
}