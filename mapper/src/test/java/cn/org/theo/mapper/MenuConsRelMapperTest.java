package cn.org.theo.mapper;

import cn.org.theo.pojo.MenuConsRel;
import cn.org.theo.vo.MenuVo;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

/**
 * project name: IntelliJ IDEA
 * Date: 2022/8/24 0:20
 *
 * @PackageName: cn.org.theo.mapper
 * @ClassName: MenuConsRelMapperTest
 * @Author: Theo
 * @Version:
 * @Description:
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:applicationContext.xml"})
public class MenuConsRelMapperTest {

    @Autowired
    private MenuConsRelMapper menuConsRelMapper;

    @Test
    public void a(){
        MenuConsRel menuConsRel = new MenuConsRel();
        menuConsRel.setStatus(2);
        System.out.println(menuConsRel);
        List<MenuVo> query = menuConsRelMapper.queryForMenuVo(menuConsRel);
        query.forEach(System.out::println);

    }

}