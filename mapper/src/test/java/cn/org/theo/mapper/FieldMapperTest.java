package cn.org.theo.mapper;

import cn.org.theo.pojo.Field;
import cn.org.theo.vo.FieldNameVo;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

/**
 * project name: IntelliJ IDEA
 * Date: 2022/8/28 13:09
 *
 * @PackageName: cn.org.theo.mapper
 * @ClassName: FieldMapperTest
 * @Author: Theo
 * @Version:
 * @Description:
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:applicationContext.xml"})
public class FieldMapperTest {

    @Autowired
    private FieldMapper fieldMapper;

    @Test
    public void queryId() {
        //["婚姻关系","情绪管理","亲子关系"]
        List<String> fieldNameVos = new ArrayList<>();
        fieldNameVos.add("婚姻关系");
        fieldNameVos.add("情绪管理");
        List<Field> fields = fieldMapper.queryId(fieldNameVos);
        fields.forEach(System.out::println);

    }
}