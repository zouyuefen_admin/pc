package cn.org.theo.mapper;

import cn.org.theo.pojo.Configure;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * project name: IntelliJ IDEA
 * Date: 2022/8/24 15:48
 *
 * @PackageName: cn.org.theo.mapper
 * @ClassName: ConfigureMapperTest
 * @Author: Theo
 * @Version:
 * @Description:
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:applicationContext.xml"})
public class ConfigureMapperTest {

    @Autowired
    private ConfigureMapper configureMapper;

    @Test
    public void query(){
        Configure configure = new Configure();
        configure.setKeyp("password");
        System.out.println(configureMapper.queryForOne(configure));
    }

}