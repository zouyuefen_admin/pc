package cn.org.theo.mapper;

import cn.org.theo.pojo.Consultant;
import cn.org.theo.pojo.Manager;
import cn.org.theo.vo.ConsultantFrontVo;
import cn.org.theo.vo.ConsultantVo;

import java.util.List;
import java.util.Map;

/**
 * project name: IntelliJ IDEA
 * Date: 2022/8/24 21:08
 *
 * @PackageName: cn.org.theo.mapper
 * @ClassName: ConsultantMapper
 * @Author: Theo
 * @Version:
 * @Description:
 */
public interface ConsultantMapper {

    List<Consultant> queryPositionalTitles();

    List<ConsultantVo> queryForConditionByPage(Map<String,Object> map);

    int queryTotalCount(ConsultantVo consultant);


    Manager query(ConsultantVo consultantVo);
    Consultant query2(Consultant consultant);
    ConsultantFrontVo query3(Consultant consultant);
    Consultant query5(int id);

    List<ConsultantVo> queryForConditionByPage2(Map<String, Object> map);

    int insert(Consultant consultant);

    List<ConsultantFrontVo> queryByFront(Map<String, Object> map);

    int queryTotalCountFront(Map<String, Object> map);


    Consultant query4(Consultant user);

    List<ConsultantFrontVo> getConsInfoByIndex();

    int queryServicePrice(int consId);
}
