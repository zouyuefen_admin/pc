package cn.org.theo.mapper;

import cn.org.theo.pojo.Menu;
import cn.org.theo.pojo.MenuConsRel;
import cn.org.theo.vo.MenuVo;

import java.util.List;

/**
 * project name: IntelliJ IDEA
 * Date: 2022/8/24 0:15
 *
 * @PackageName: cn.org.theo.mapper
 * @ClassName: MenuConsRelMapper
 * @Author: Theo
 * @Version:
 * @Description:
 */
public interface MenuConsRelMapper {

    List<MenuConsRel> query(MenuConsRel menuConsRel);

    List<MenuVo> queryForMenuVo(MenuConsRel menuConsRel);

    List<Menu> getList(int authority);
}
