package cn.org.theo.mapper;

import cn.org.theo.pojo.Manager;
import cn.org.theo.pojo.Schedule;
import cn.org.theo.vo.ScheduleVo;

import java.util.List;

/**
 * project name: IntelliJ IDEA
 * Date: 2022/8/25 16:41
 *
 * @PackageName: cn.org.theo.mapper
 * @ClassName: ScheduleMapper
 * @Author: Theo
 * @Version:
 * @Description:
 */
public interface ScheduleMapper {

    int insert(Schedule schedule);


    List<ScheduleVo> query(Manager manager);
}
