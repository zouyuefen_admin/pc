package cn.org.theo.mapper;

import cn.org.theo.pojo.Evaluate;

/**
 * project name: IntelliJ IDEA
 * Date: 2022/8/29 19:06
 *
 * @PackageName: cn.org.theo.mapper
 * @ClassName: EvaluateMapper
 * @Author: Theo
 * @Version:
 * @Description:
 */
public interface EvaluateMapper {


    Evaluate query(Evaluate evaluateQuery);

    int insert(String subId);

    int update(Evaluate evaluate1);
}
