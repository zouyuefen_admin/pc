package cn.org.theo.mapper;

import cn.org.theo.pojo.Journal;

/**
 * project name: IntelliJ IDEA
 * Date: 2022/8/24 15:15
 *
 * @PackageName: cn.org.theo.mapper
 * @ClassName: JournalMapper
 * @Author: Theo
 * @Version:
 * @Description:
 */
public interface JournalMapper {
    int add(Journal journal);
}
