package cn.org.theo.mapper;

import cn.org.theo.pojo.Page;
import cn.org.theo.pojo.User;
import cn.org.theo.qo.Week;
import cn.org.theo.vo.MenuVo;
import cn.org.theo.vo.WeekData;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * project name: IntelliJ IDEA
 * Date: 2022/8/21 13:07
 *
 * @PackageName: cn.org.theo.mapper
 * @ClassName: UserMapper
 * @Author: Theo
 * @Version:
 * @Description: UserMapper
 */
public interface UserMapper {

    int addUser(User user);

    User query(User user);
    int queryTotalCount(User user);

    int update(User user);

    int delete(int id);

    List<User> queryByPage(Page<User> page);
    List<User> queryForConditionByPage(Map<String,Object> map);

    List<User> queryForConditionByPage2(Map<String, Object> map);

    int updateBalance(User user);

    int costMoney(HashMap<String,Object> map);

    List<WeekData> queryForWeekData(List<String> list);

    List<WeekData> queryForMonthData(List<Integer> list);

    List<WeekData> queryForYearData(List<Integer> list);
}
