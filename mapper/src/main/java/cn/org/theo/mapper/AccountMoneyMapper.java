package cn.org.theo.mapper;

import cn.org.theo.pojo.AccountMoney;
import cn.org.theo.vo.AccountMoneyVo;

import java.util.HashMap;
import java.util.List;

/**
 * project name: IntelliJ IDEA
 * Date: 2022/9/5 9:48
 *
 * @PackageName: cn.org.theo.mapper
 * @ClassName: AccountMoneyMapper
 * @Author: Theo
 * @Version:
 * @Description:
 */
public interface AccountMoneyMapper {


    int insert(AccountMoney accountMoney);

    List<AccountMoneyVo> getAccountMoneyList(HashMap<String, Object> map);

    int queryTotalCount(HashMap<String, Object> map);

    AccountMoneyVo queryOne(int id);

    List<AccountMoneyVo> getAccountMoneyListForBack(HashMap<String, Object> map);

    int queryTotalCountForBack(HashMap<String, Object> map);
}
