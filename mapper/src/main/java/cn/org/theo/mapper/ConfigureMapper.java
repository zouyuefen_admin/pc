package cn.org.theo.mapper;

import cn.org.theo.pojo.Configure;

/**
 * project name: IntelliJ IDEA
 * Date: 2022/8/24 15:45
 *
 * @PackageName: cn.org.theo.mapper
 * @ClassName: ConfigureMapper
 * @Author: Theo
 * @Version:
 * @Description:
 */
public interface ConfigureMapper {

    Configure queryForOne(Configure configure);

}
