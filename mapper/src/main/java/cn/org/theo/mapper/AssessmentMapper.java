package cn.org.theo.mapper;

import cn.org.theo.pojo.Assessment;
import cn.org.theo.pojo.AssessmentVo;

import java.util.HashMap;
import java.util.List;

/**
 * project name: IntelliJ IDEA
 * Date: 2022/9/1 14:47
 *
 * @PackageName: cn.org.theo.mapper
 * @ClassName: AssessmentMapper
 * @Author: Theo
 * @Version:
 * @Description:
 */
public interface AssessmentMapper {
    int insert(Assessment assessment);

    List<AssessmentVo> queryForConditionByPage(HashMap<String, Object> map);

    int queryTotalCount(HashMap<String, Object> map);

    AssessmentVo queryOne(int id);

    List<AssessmentVo> queryForConditionByPageFront(HashMap<String, Object> map);

    int queryTotalCountFront(HashMap<String, Object> map);


}
