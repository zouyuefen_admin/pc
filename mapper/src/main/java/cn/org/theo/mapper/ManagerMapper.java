package cn.org.theo.mapper;

import cn.org.theo.pojo.Manager;

/**
 * project name: IntelliJ IDEA
 * Date: 2022/8/23 21:30
 *
 * @PackageName: cn.org.theo.mapper
 * @ClassName: ManagerMapper
 * @Author: Theo
 * @Version:
 * @Description:
 */
public interface ManagerMapper {

    Manager query(Manager manager);

    int update(Manager manager);

    int getMaxNum();

    int insert(Manager manager);
}
