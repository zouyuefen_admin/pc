package cn.org.theo.mapper;

import cn.org.theo.pojo.OrderStatus;

import java.util.List;

/**
 * project name: IntelliJ IDEA
 * Date: 2022/8/26 10:04
 *
 * @PackageName: cn.org.theo.mapper
 * @ClassName: OrderStatusMapper
 * @Author: Theo
 * @Version:
 * @Description:
 */
public interface OrderStatusMapper {


    List<OrderStatus> query();
}
