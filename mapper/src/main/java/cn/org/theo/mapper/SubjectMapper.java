package cn.org.theo.mapper;

import cn.org.theo.pojo.DemoData;
import cn.org.theo.pojo.Subject;
import cn.org.theo.vo.SubjectVo;

import java.util.List;
import java.util.Map;

/**
 * project name: IntelliJ IDEA
 * Date: 2022/8/31 10:35
 *
 * @PackageName: cn.org.theo.mapper
 * @ClassName: SubjectMapper
 * @Author: Theo
 * @Version:
 * @Description:
 */
public interface SubjectMapper {
    List<Subject> queryBackPage(Map<String, Object> map);

    int getTotalCount(Map<String, Object> map);

    SubjectVo queryOne(Map<String, Object> map);

    int update(SubjectVo subjectVo);

    int delete(SubjectVo subjectVo);

    int add(SubjectVo subjectVo);

    int addList(List<DemoData> list);

    int queryMaxColId(Integer fieldId);

    int queryTotalCount(Integer fieldId);
}
