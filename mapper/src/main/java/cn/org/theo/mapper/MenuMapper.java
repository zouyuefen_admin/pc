package cn.org.theo.mapper;

import cn.org.theo.pojo.Menu;
import cn.org.theo.qo.MenuQo;
import cn.org.theo.vo.MenuVo;

import java.util.List;
import java.util.Map;

/**
 * project name: IntelliJ IDEA
 * Date: 2022/9/5 16:43
 *
 * @PackageName: cn.org.theo.mapper
 * @ClassName: MenuMapper
 * @Author: Theo
 * @Version:
 * @Description:
 */
public interface MenuMapper {


    List<Menu> queryOneLevel();

    List<MenuVo> queryForConditionByBackPage(Map<String, Object> map);

    int getTotalCountBackPage(Map<String, Object> map);

    int queryMid(String parentName);

    int add(Menu menu);

    MenuVo queryOne(int id);

    int update(Menu menu);

    int delete(MenuQo menuQo);
    List<Menu> getList(int i);

    List<Menu> getList2(int i);

    Menu queryOneForName(Menu menu);

    int updateAuthority(Menu menu);

    int updateAuthorityForList(Menu menu);

    int updateAuthorityForList2(Menu menu);
}
