package cn.org.theo.mapper;

import cn.org.theo.vo.ConsultantFieldVo;

import java.util.List;

/**
 * project name: IntelliJ IDEA
 * Date: 2022/8/27 11:03
 *
 * @PackageName: cn.org.theo.mapper
 * @ClassName: ConsultantFieldMapper
 * @Author: Theo
 * @Version:
 * @Description:
 */
public interface ConsultantFieldMapper {

    List<ConsultantFieldVo> query(int consId);

    List<Integer> queryId(List<Integer> fieldId);
}
