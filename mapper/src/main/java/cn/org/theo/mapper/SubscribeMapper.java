package cn.org.theo.mapper;

import cn.org.theo.pojo.Subscribe;
import cn.org.theo.qo.DateVo;
import cn.org.theo.qo.SubscribeQo;
import cn.org.theo.vo.OrderListVo;
import cn.org.theo.vo.SubscribeForBackVo;
import cn.org.theo.vo.SubscribeVo;
import cn.org.theo.vo.WeekData;

import java.util.List;
import java.util.Map;

/**
 * project name: IntelliJ IDEA
 * Date: 2022/8/26 10:58
 *
 * @PackageName: cn.org.theo.mapper
 * @ClassName: SubscribeMapper
 * @Author: Theo
 * @Version:
 * @Description:
 */
public interface SubscribeMapper {
    List<SubscribeVo> queryForConditionByPage(Map<String, Object> map);

    int queryTotalCount(Map<String, Object> map);

    int insert(Subscribe subscribe);

    List<OrderListVo> queryForFront(Map<String, Object> map);

    int queryTotalCountFront(Map<String, Object> map);

    int update(int id);
    int update3(int id);
    int update2(String orderId);

    Subscribe queryOne(Subscribe subscribe);
    SubscribeForBackVo queryOne2(SubscribeForBackVo subscribe);

    int updateForDiagnosis(Subscribe subscribe);

    OrderListVo queryForBack(int id);

    List<SubscribeForBackVo> queryForConditionByBackPage(Map<String, Object> map);

    int getTotalCountBackPage(Map<String, Object> map);

    List<WeekData> queryForStatistics(DateVo dateVo);
}
