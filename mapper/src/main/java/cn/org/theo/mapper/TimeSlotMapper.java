package cn.org.theo.mapper;

import cn.org.theo.pojo.TimeSlot;

/**
 * project name: IntelliJ IDEA
 * Date: 2022/8/25 16:29
 *
 * @PackageName: cn.org.theo.mapper
 * @ClassName: TimeSlotMapper
 * @Author: Theo
 * @Version:
 * @Description:
 */
public interface TimeSlotMapper {

    int query(String slot);

}
