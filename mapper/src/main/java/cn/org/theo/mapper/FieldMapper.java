package cn.org.theo.mapper;

import cn.org.theo.pojo.Field;
import cn.org.theo.vo.FieldNameVo;

import java.util.List;

/**
 * project name: IntelliJ IDEA
 * Date: 2022/8/27 15:52
 *
 * @PackageName: cn.org.theo.mapper
 * @ClassName: FieldMapper
 * @Author: Theo
 * @Version:
 * @Description:
 */
public interface FieldMapper {

    List<Field> query();

    List<Field> queryId(List<String> fieldNameVos);

    int queryId2(String fieldName);
}
