# 心理咨询平台

#### 介绍
心理咨询平台项目，这是一个前后端分离的项目，前端使用html，css,js,jq，zui框架。ajax发送请求。后端使用ssm框架。

#### 软件架构
软件架构说明


#### 安装教程

1. 后台登录系统
![img.png](img.png)
![img_1.png](img_1.png)
![img_2.png](img_2.png)
2. 前台界面
![img_3.png](img_3.png)
![img_4.png](img_4.png)
![img_5.png](img_5.png)
前台咨询师列表
![img_6.png](img_6.png)
在线心理测试
![img_7.png](img_7.png)
测试报告
![img_8.png](img_8.png)
收支明细
![img_9.png](img_9.png)


#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
