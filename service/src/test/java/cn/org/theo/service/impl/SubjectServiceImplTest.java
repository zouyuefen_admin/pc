package cn.org.theo.service.impl;

import cn.org.theo.pojo.Result;
import cn.org.theo.service.SubjectService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.*;

/**
 * project name: IntelliJ IDEA
 * Date: 2022/8/31 10:44
 *
 * @PackageName: cn.org.theo.service.impl
 * @ClassName: SubjectServiceImplTest
 * @Author: Theo
 * @Version:
 * @Description:
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:applicationContext.xml"})
public class SubjectServiceImplTest {

    @Autowired
    private SubjectService subjectService;

    @Test
    public void getList() {
        Result list = subjectService.getList(1, 1);
        System.out.println(list);
    }

    @Test
    public void addOneSubject() {


    }
}