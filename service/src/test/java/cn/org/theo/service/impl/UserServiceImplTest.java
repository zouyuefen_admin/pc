package cn.org.theo.service.impl;

import cn.org.theo.pojo.Result;
import cn.org.theo.pojo.User;
import cn.org.theo.service.UserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.*;

/**
 * project name: IntelliJ IDEA
 * Date: 2022/8/21 14:24
 *
 * @PackageName: cn.org.theo.service.impl
 * @ClassName: UserServiceImplTest
 * @Author: Theo
 * @Version:
 * @Description:
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:applicationContext.xml"})
public class UserServiceImplTest {
    @Autowired
    private UserService userService;

    @Test
    public void tes(){
        User user = new User();
        user.setUsername("Theo");
        Result reg = userService.reg(user);
        System.out.println(reg);
    }

    @Test
    public void updateStatus() {
        User user = new User();
        user.setId(11);
        userService.updateStatus(user,"admin");
    }
}