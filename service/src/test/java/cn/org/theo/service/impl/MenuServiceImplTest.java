package cn.org.theo.service.impl;

import cn.org.theo.service.MenuService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.*;

/**
 * project name: IntelliJ IDEA
 * Date: 2022/9/6 12:39
 *
 * @PackageName: cn.org.theo.service.impl
 * @ClassName: MenuServiceImplTest
 * @Author: Theo
 * @Version:
 * @Description:
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:applicationContext.xml"})
public class MenuServiceImplTest {
    @Autowired
    private MenuService menuService;

    @Test
    public void getManagerList() {
        menuService.getManagerList(1);
    }
}