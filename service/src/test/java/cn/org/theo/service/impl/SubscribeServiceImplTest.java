package cn.org.theo.service.impl;

import cn.org.theo.mapper.SubscribeMapper;
import cn.org.theo.pojo.Result;
import cn.org.theo.qo.SubscribeForBackQo;
import cn.org.theo.service.SubscribeService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.*;

/**
 * project name: IntelliJ IDEA
 * Date: 2022/8/30 20:14
 *
 * @PackageName: cn.org.theo.service.impl
 * @ClassName: SubscribeServiceImplTest
 * @Author: Theo
 * @Version:
 * @Description:
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:applicationContext.xml"})
public class SubscribeServiceImplTest {

    @Autowired
    SubscribeService subscribeService;
    @Test
    public void getSubList() {
        SubscribeForBackQo subscribeForBackQo = new SubscribeForBackQo();
        Result subList = subscribeService.getSubList(subscribeForBackQo, 1);
        System.out.println(subList.getData());

    }
}