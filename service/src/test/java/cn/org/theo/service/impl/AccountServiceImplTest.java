package cn.org.theo.service.impl;

import cn.org.theo.pojo.Result;
import cn.org.theo.service.AccountService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.*;

/**
 * project name: IntelliJ IDEA
 * Date: 2022/9/2 16:44
 *
 * @PackageName: cn.org.theo.service.impl
 * @ClassName: AccountServiceImplTest
 * @Author: Theo
 * @Version:
 * @Description:
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:applicationContext.xml"})

public class AccountServiceImplTest {

    @Autowired
    private AccountService accountService;

    @Test
    public void  a(){
        Result theo666 = accountService.getAccountInfo("Theo666");
        System.out.println(theo666);
    }

}