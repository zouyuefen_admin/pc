package cn.org.theo.service.impl;

import cn.org.theo.mapper.ConsultantMapper;
import cn.org.theo.pojo.Result;
import cn.org.theo.service.ConsultantService;
import cn.org.theo.vo.ConsultantFieldVo;
import cn.org.theo.vo.ConsultantVo;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

import static org.junit.Assert.*;

/**
 * project name: IntelliJ IDEA
 * Date: 2022/8/27 10:32
 *
 * @PackageName: cn.org.theo.service.impl
 * @ClassName: ConsultantServiceImplTest
 * @Author: Theo
 * @Version:
 * @Description:
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:applicationContext.xml"})
public class ConsultantServiceImplTest {

    @Autowired
    private ConsultantService consultantService;



    @Test
    public void queryByFront() {
//        consultantService.queryByFront(new ConsultantVo(), 1);
    }

    @Test
    public void timeInfo() {
        Result result = consultantService.timeInfo(1);
        System.out.println(result.getData());

    }
}