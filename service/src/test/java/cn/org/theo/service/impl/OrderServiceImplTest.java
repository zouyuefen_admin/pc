package cn.org.theo.service.impl;

import cn.org.theo.pojo.Result;
import cn.org.theo.qo.SubscribeFrontQo;
import cn.org.theo.qo.SubscribeQo;
import cn.org.theo.service.OrderService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import static org.junit.Assert.*;

/**
 * project name: IntelliJ IDEA
 * Date: 2022/8/26 11:18
 *
 * @PackageName: cn.org.theo.service.impl
 * @ClassName: OrderServiceImplTest
 * @Author: Theo
 * @Version:
 * @Description:
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:applicationContext.xml"})
public class OrderServiceImplTest {

    @Autowired
    private OrderService orderService;
    @Test
    public void getListForPage() {
        SubscribeQo subscribeQo = new SubscribeQo();
        subscribeQo.setConsName("tea5");
        Result listForPage = orderService.getListForPage(subscribeQo, 1);
        System.out.println(listForPage);
    }

    @Test
    public void testGetListForPage() {
        SubscribeQo subscribeQo = new SubscribeQo();
        subscribeQo.setConsName("tea5");
        Result listForPage = orderService.getListForPage(subscribeQo, 1);
        System.out.println(listForPage);

        // 首先设置"Mon Dec 28 00:00:00 CST 2008"的格式,用来将其转化为Date对象
        DateFormat df = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy", Locale.US);
        //将已有的时间字符串转化为Date对象
        try {
            Date date = df.parse("Fri Aug 26 00:00:00 CST 2022");// 那天是周一
            // 创建所需的格式
            df = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            String str = df.format(date);// 获得格式化后的日期字符串
            System.err.println(str);// 打印最终结果
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }


    }

    @Test
    public void createOrder() {

        SubscribeFrontQo s = new SubscribeFrontQo();
        s.setUsername("Theo666");

        s.setDataTime( new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date(System.currentTimeMillis())));
        s.setConsId(1);
        s.setQuestion("写代码写疯了");
        s.setFieldName("情绪管理");
        Result order = orderService.createOrder(s);
        System.out.println(order);


    }
    @Test
    public void showOrder() {

        Result theo666 = orderService.showOrder("Theo666", 1);


    }

    @Test
    public void subscribeInfoForOne() {
        Result result = orderService.subscribeInfoForOne(18);

    }

    @Test
    public void testShowOrder() {
        Result theo666 = orderService.showOrder("Theo666", 2);
        System.out.println(theo666);
    }
}