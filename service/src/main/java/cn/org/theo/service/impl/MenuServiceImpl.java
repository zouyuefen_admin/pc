package cn.org.theo.service.impl;

import cn.org.theo.mapper.MenuConsRelMapper;
import cn.org.theo.mapper.MenuMapper;
import cn.org.theo.pojo.*;
import cn.org.theo.qo.MenuQo;
import cn.org.theo.service.MenuService;
import cn.org.theo.vo.MenuVo;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * project name: IntelliJ IDEA
 * Date: 2022/9/5 16:41
 *
 * @PackageName: cn.org.theo.service.impl
 * @ClassName: MenuServiceImpl
 * @Author: Theo
 * @Version:
 * @Description:
 */
@Service
public class MenuServiceImpl implements MenuService {

    @Autowired
    private MenuMapper menuMapper;
    @Autowired
    private MenuConsRelMapper menuConsRelMapper;

    @Override
    public Result getMenuList() {
        List<Menu> menuList = menuMapper.queryOneLevel();
        return new Result(0,
                "一级菜单获取成功",menuList);
    }

    @Override
    public Result getMenuListForCondition(MenuQo menuQo) {
        System.out.println(menuQo);
        String menuName1= menuQo.getMenuName();
        String parentName = menuQo.getParentName();
        if (menuName1 == null || "".equals(menuName1)){
            menuName1 = null;
        }
        if (parentName == null || "".equals(parentName)){
            parentName = null;
        }
        Map<String,Object> map = new HashMap<>();
        map.put("parentName",parentName);
        map.put("menuName",menuName1);
        Integer pageNo = menuQo.getPageNo();
        map.put("limit",(pageNo - 1) * 5);
        map.put("offset",5);

        List<MenuVo> menuVos = menuMapper.queryForConditionByBackPage(map);
        Page<MenuVo> page = new Page();
        page.setPageNo(pageNo);
        page.setLimit((pageNo - 1) * 5);
        page.setOffset(5);
        page.setCondition(true);
        page.setList(menuVos);
        page.setTotalCount(menuMapper.getTotalCountBackPage(map));
        System.out.println(menuMapper.getTotalCountBackPage(map));
        int totalCount = menuMapper.getTotalCountBackPage(map);
        page.setTotalPageCount(totalCount % 5 == 0 ? totalCount / 5 : totalCount / 5 + 1);
        if (page.getTotalPageCount() == 0) {
            page.setCurrentPage(0);
        }
        return Result.updateResp(1,"数据获取成功","数据获取失败",page);
    }

    @Override
    public Result add(MenuQo menuQo) {
        Menu menu = new Menu();
        menu.setName(menuQo.getMenuName());
        menu.setHref(menuQo.getUrl());
        if (menuQo.getParentName() != null && !"".equals(menuQo.getParentName())){
            int mid = menuMapper.queryMid(menuQo.getParentName());
            menu.setMid(mid);
            menu.setIsParent(0);
            menu.setAuthority(2);
        }else{
            menu.setMid(0);
            menu.setIsParent(1);
            menu.setAuthority(-1);
        }
        int i = menuMapper.add(menu);

        return Result.updateResp(i,"添加成功","添加失败","");
    }

    @Override
    public Result getOneMenu(int id) {
        MenuVo menuVo = menuMapper.queryOne(id);

        return new Result(0,"数据获取成功",menuVo);
    }

    @Override
    public Result update(MenuQo menuQo) {
        Menu menu = new Menu();
        menu.setName(menuQo.getMenuName());
        menu.setId(menuQo.getId());
        menu.setHref(menuQo.getUrl());
        if (menuQo.getParentName() != null && !"".equals(menuQo.getParentName())){
            int mid = menuMapper.queryMid(menuQo.getParentName());
            menu.setMid(mid);
            menu.setIsParent(0);
        }else{
            menu.setMid(0);
            menu.setIsParent(1);
        }
        int i = menuMapper.update(menu);

        return Result.updateResp(i,"修改成功","修改失败",null);
    }

    @Override
    public Result delete(MenuQo menuQo) {
        int i = menuMapper.delete(menuQo);
        return Result.updateResp(i,"删除成功","删除失败",null);
    }

    @Override
    public Result getManagerList(int authority) {

        List<Menu> menuList = menuMapper.getList(authority);
        menuList.forEach(System.out::println);

        HashMap<String ,List<String>> map = new HashMap<>();

        List<Menu> parentList = menuMapper.getList(-1);

        for (Menu menu : parentList) {
            Integer id = menu.getId();
            List<String> list = new ArrayList<>();
            for (Menu menu1 : menuList) {
                if (menu1.getMid() == id){
                    list.add(menu1.getName());
                }
            }
            map.put(menu.getName(),list);
        }
        for (Map.Entry<String, List<String>> entry : map.entrySet()) {
            System.out.println("key = " + entry.getKey() + ", value = " + entry.getValue());
        }
        JSONObject itemJSONObj = JSONObject.parseObject(JSON.toJSONString(map));
        System.out.println(itemJSONObj.toString());
        return new Result(0,"数据获取成功",itemJSONObj.toString());
    }

    /**
     * 获取未分配菜单
     * @param authority
     * @return
     */
    @Override
    public Result getManagerList2(int authority) {

        List<Menu> menuList = menuMapper.getList2(authority);
        menuList.forEach(System.out::println);

        HashMap<String ,List<String>> map = new HashMap<>();

        List<Menu> parentList = menuMapper.getList(-1);

        for (Menu menu : parentList) {
            Integer id = menu.getId();
            List<String> list = new ArrayList<>();
            for (Menu menu1 : menuList) {
                if (menu1.getMid() == id){
                    list.add(menu1.getName());
                }
            }

                map.put(menu.getName(),list);
        }
        for (Map.Entry<String, List<String>> entry : map.entrySet()) {
            System.out.println("key = " + entry.getKey() + ", value = " + entry.getValue());
        }
        JSONObject itemJSONObj = JSONObject.parseObject(JSON.toJSONString(map));
        System.out.println(itemJSONObj.toString());
        return new Result(0,"数据获取成功",itemJSONObj.toString());
    }

    @Override
    public Result singleRight(Menu menu) {
        //判断是否是右边的
        Menu menu1 = menuMapper.queryOneForName(menu);
        //判断是否是父级菜单
        if (menu1.getIsParent() == 1){
            return new Result(-1,"父级菜单不能使用单箭头移动！",0);
        }
        if (menu1.getAuthority() != menu.getAuthority()){
            return new Result(-1,"未分配菜单项不能进行右移！",0);
        }
        menu.setAuthority(2);
        int i = menuMapper.updateAuthority(menu);
        return Result.updateResp(i,"移动成功","移动失败",i);
    }

    @Override
    public Result singleLeft(Menu menu) {
        Menu menu1 = menuMapper.queryOneForName(menu);
        System.out.println(menu1);
        //判断是否是父级菜单
        if (menu1.getIsParent() == 1){
            return new Result(-1,"父级菜单不能使用单箭头移动！",0);
        }
        //判断是否是右边的//2 1/0
        if (menu1.getAuthority() == menu.getAuthority()){
            return new Result(-1,"已分配菜单项不能进行左移！",0);
        }
        //判断是否已分配
        if (menu1.getAuthority() != 2){
            return new Result(-1,"该菜单项已分配！",0);

        }
        menu.setAuthority(menu.getAuthority());
        int i = menuMapper.updateAuthority(menu);
        return Result.updateResp(i,"移动成功","移动失败",i);
    }

    @Override
    public Result doubleLeft(Menu menu) {
//        Menu menu1 = menuMapper.queryOneForName(menu);
//        System.out.println(menu1);
        //判断是否是父级菜单
//        if (menu1.getIsParent() != 1){
//            return new Result(-1,"子菜单不能使用双箭头移动！",0);
//        }
        //将mid = menu1.id 的菜单项的authori修改为 1/0
        int i = menuMapper.updateAuthorityForList(menu);
        return Result.updateResp(i>0 ? 1:0,"移动成功","移动失败",i);
    }

    @Override
    public Result doubleRight(Menu menu) {
        int i = menuMapper.updateAuthorityForList2(menu);
        return Result.updateResp(i>0 ? 1:0,"移动成功","移动失败",i);
    }
}
