package cn.org.theo.service.impl;

import cn.org.theo.mapper.ConfigureMapper;
import cn.org.theo.mapper.JournalMapper;
import cn.org.theo.mapper.ManagerMapper;
import cn.org.theo.mapper.UserMapper;
import cn.org.theo.pojo.*;
import cn.org.theo.service.ManagerService;
import cn.org.theo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * project name: IntelliJ IDEA
 * Date: 2022/8/21 13:39
 *
 * @PackageName: cn.org.theo.service.impl
 * @ClassName: UserServiceImpl
 * @Author: Theo
 * @Version:
 * @Description:
 */
@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserMapper userMapper;

    @Autowired
    private ManagerMapper managerMapper;
    @Autowired
    private JournalMapper journalMapper;

    @Autowired
    private ConfigureMapper configureMapper;
    @Override
    public Result reg(User user) {
        User user2 = new User();
        user2.setUsername(user.getUsername());
        user.setBalance(0);
        user.setRegTime(new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date(System.currentTimeMillis())));
        user.setStatus(1);
        User user1 = userMapper.query(user2);
        if (user1 != null){
            return new Result(-1,"注册失败，用户已存在！",null);
        }
        int i = userMapper.addUser(user);
        return Result.updateResp(i,"注册成功！","注册失败",null);
    }

    @Override
    public int login(User user) {

        User user1 = userMapper.query(user);
        if (user1 != null){
            if (user1.getStatus() == 3){
                System.out.println(user1);
                return 2;
            }
            return 1;
        }else{

            return 0;
        }
    }

    @Override
    public Result initData(int pageNo) {
        Page<User> page = new Page();
        page.setPageNo(pageNo);
        page.setLimit((pageNo - 1) * 5);
        page.setOffset(5);
        page.setCondition(false);
        List<User> users = userMapper.queryByPage(page);
        page.setList(users);
        page.setTotalCount(userMapper.queryTotalCount(new User()));
        page.setCurrentPage(pageNo);
        int totalCount = page.getTotalCount();
        page.setTotalPageCount(totalCount % 5 == 0 ? totalCount / 5 : totalCount / 5 + 1);
        if (page.getTotalPageCount() == 0) {
            page.setCurrentPage(0);
        }
        return Result.updateResp(1, "获取数据成功", "", page);
    }
    @Override
    public Result updateStatus(User user, String username) {
//        user.setStatus(3);
        User query = userMapper.query(user);
        user.setStatus(query.getStatus() == 1 ? 3:1);
        int i = userMapper.update(user);
        //插入操作日志
        if (username != null){
            Manager manager = new Manager();
            manager.setUsername(username);
            Manager query1 = managerMapper.query(manager);
            Journal journal = new Journal();
            journal.setUserId(query1.getId());
            long l = System.currentTimeMillis();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            journal.setTime(sdf.format(l));
            journal.setOption(query.getStatus() == 1 ? "禁用用户" + query.getUsername():"启用用户" + query.getUsername());
            journalMapper.add(journal);
        }

        return Result.updateResp(i,"状态修改成功","状态修改失败",null);
    }

    @Override
    public Result delete(int id,String username) {
        //逻辑删除
        User user = new User();
        user.setId(id);
        user.setStatus(2);
        int delete = userMapper.update(user);
        //插入操作日志
        Manager manager = new Manager();
        manager.setUsername(username);
        if (username != null){
            Manager query1 = managerMapper.query(manager);
            Journal journal = new Journal();
            journal.setUserId(query1.getId());
            long l = System.currentTimeMillis();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            journal.setTime(sdf.format(l));
            journal.setOption("删除用户id为" + id);
            journalMapper.add(journal);
        }

        return Result.updateResp(delete,"删除成功","删除失败",null);
    }

    @Override
    public Result updatePwd(int id,String username) {
        User user = new User();
        user.setId(id);
        //获取参数表默认密码
        Configure configure = new Configure();
        configure.setKeyp("password");
        Configure configure1 = configureMapper.queryForOne(configure);
        user.setPassword(configure1.getValue());
        int update = userMapper.update(user);
        //插入操作日志
        if (username != null) {
            Manager manager = new Manager();
            manager.setUsername(username);
            Manager query1 = managerMapper.query(manager);

            Journal journal = new Journal();
            journal.setUserId(query1.getId());
            long l = System.currentTimeMillis();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            journal.setTime(sdf.format(l));
            journal.setOption("id为" + id + "用户重置密码");
            journalMapper.add(journal);
        }
        return Result.updateResp(update, "重置密码成功", "重置密码失败", null);
    }

    @Override
    public Result queryForCondition(User user, int pageNo) {
        Map<String,Object> map = new HashMap<>();
        System.out.println(user.getStatus());
        if (user.getStatus() == 1){
            map.put("username","%"+user.getUsername()+"%");
            map.put("status",user.getStatus());
            map.put("limit",(pageNo - 1) * 5);
            map.put("offset",5);
            List<User> users = userMapper.queryForConditionByPage(map);
            Page<User> page = new Page();
            page.setPageNo(pageNo);
            page.setLimit((pageNo - 1) * 5);
            page.setOffset(5);
            page.setCondition(true);
            page.setList(users);
            user.setUsername("%"+user.getUsername()+"%");
            page.setTotalCount(userMapper.queryTotalCount(user));
            System.out.println(page.getTotalCount());
            int totalCount = page.getTotalCount();
            page.setTotalPageCount(totalCount % 5 == 0 ? totalCount / 5 : totalCount / 5 + 1);
            if (page.getTotalPageCount() == 0) {
                page.setCurrentPage(0);
            }
            return Result.updateResp(1,"数据获取成功","数据获取失败",page);
        }else if (user.getStatus() == 3){
            map.put("username","%"+user.getUsername()+"%");
            //查询status不等于1的数据
            map.put("status",1);
            map.put("limit",(pageNo - 1) * 5);
            map.put("offset",5);
            List<User> users = userMapper.queryForConditionByPage2(map);
            Page<User> page = new Page();
            page.setPageNo(pageNo);
            page.setLimit((pageNo - 1) * 5);
            page.setOffset(5);
            page.setCondition(true);
            page.setList(users);
            user.setUsername("%"+user.getUsername()+"%");
            page.setTotalCount(userMapper.queryTotalCount(user));
            System.out.println(page.getTotalCount());
            int totalCount = page.getTotalCount();
            page.setTotalPageCount(totalCount % 5 == 0 ? totalCount / 5 : totalCount / 5 + 1);
            if (page.getTotalPageCount() == 0) {
                page.setCurrentPage(0);
            }
            return Result.updateResp(1,"数据获取成功","数据获取失败",page);
        }
        return Result.updateResp(0,"数据获取成功","数据获取失败",null);
    }

}
