package cn.org.theo.service;

import cn.org.theo.pojo.Manager;
import cn.org.theo.pojo.Result;

/**
 * project name: IntelliJ IDEA
 * Date: 2022/8/23 21:39
 *
 * @PackageName: cn.org.theo.service
 * @ClassName: ManagerService
 * @Author: Theo
 * @Version:
 * @Description:
 */
public interface ManagerService {

    int login(Manager manager);

    Result getAdminInfo(String username);


}
