package cn.org.theo.service.impl;

import cn.org.theo.mapper.AssessmentMapper;
import cn.org.theo.mapper.UserMapper;
import cn.org.theo.pojo.*;
import cn.org.theo.qo.AssessmentQo;
import cn.org.theo.service.AssessmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;

/**
 * project name: IntelliJ IDEA
 * Date: 2022/9/1 16:26
 *
 * @PackageName: cn.org.theo.service.impl
 * @ClassName: AssessmentServiceImpl
 * @Author: Theo
 * @Version:
 * @Description:
 */
@Service
public class AssessmentServiceImpl implements AssessmentService {

    @Autowired
    private AssessmentMapper assessmentMapper;
    @Autowired
    private UserMapper userMapper;

    @Override
    public Result queryForCondition(AssessmentQo assessmentQo, int pageNo) {
        HashMap<String,Object> map = new HashMap<>();
        System.out.println();
        if (assessmentQo.getDate1() == null || "".equals(assessmentQo.getDate1())){
            assessmentQo.setDate1(null);
        }
        if (assessmentQo.getDate2() == null || "".equals(assessmentQo.getDate2())){
            assessmentQo.setDate2(null);
        }
        map.put("score1",assessmentQo.getScore1());
        map.put("score2",assessmentQo.getScore2());
        map.put("date1",assessmentQo.getDate1());
        map.put("date2",assessmentQo.getDate2());


        map.put("limit",(pageNo - 1) * 5);
        map.put("offset",5);

        List<AssessmentVo> assess = assessmentMapper.queryForConditionByPage(map);
        Page<AssessmentVo> page = new Page();
        page.setPageNo(pageNo);
        page.setLimit((pageNo - 1) * 5);
        page.setOffset(5);
        page.setCondition(true);
        page.setList(assess);
        page.setTotalCount(assessmentMapper.queryTotalCount(map));
        System.out.println(page.getTotalCount());
        int totalCount = page.getTotalCount();
        page.setTotalPageCount(totalCount % 5 == 0 ? totalCount / 5 : totalCount / 5 + 1);
        if (page.getTotalPageCount() == 0) {
            page.setCurrentPage(0);
        }
        return Result.updateResp(1,"数据获取成功","数据获取失败",page);
    }

    @Override
    public Result assessmentForOne(int id) {
        AssessmentVo vo =  assessmentMapper.queryOne(id);
        int i = vo != null ? 1:0;
        return Result.updateResp(i,"获取成功","获取失败",vo);
    }

    @Override
    public Result queryForConditionByFront(int pageNo, String username) {
        HashMap<String,Object> map = new HashMap<>();
        if (username == null || "".equals(username)){
            return  Result.updateResp(0,"","请先登录",null);
        }
        User user = new User();
        user.setUsername(username);
        User query = userMapper.query(user);
        map.put("uid",query.getId());
        map.put("limit",(pageNo - 1) * 8);
        map.put("offset",8);

        List<AssessmentVo> assess = assessmentMapper.queryForConditionByPageFront(map);
        Page<AssessmentVo> page = new Page();
        page.setPageNo(pageNo);
        page.setLimit((pageNo - 1) * 8);
        page.setOffset(8);
        page.setCondition(true);
        page.setList(assess);
        page.setTotalCount(assessmentMapper.queryTotalCountFront(map));
        System.out.println(page.getTotalCount());
        int totalCount = page.getTotalCount();
        page.setTotalPageCount(totalCount % 8 == 0 ? totalCount / 8 : totalCount / 8 + 1);
        if (page.getTotalPageCount() == 0) {
            page.setCurrentPage(0);
        }
        return Result.updateResp(1,"数据获取成功","数据获取失败",page);
    }

}
