package cn.org.theo.service;

import cn.org.theo.pojo.Result;
import cn.org.theo.pojo.User;

import java.util.List;

/**
 * project name: IntelliJ IDEA
 * Date: 2022/8/21 13:38
 *
 * @PackageName: cn.org.theo.service
 * @ClassName: UserService
 * @Author: Theo
 * @Version:
 * @Description:
 */

public interface UserService {

    Result reg(User user);

    int login(User user);

    Result initData(int PageNo);

    Result updateStatus(User user,String username);

    Result delete(int id,String username);

    Result updatePwd(int id,String username);
    Result queryForCondition(User user,int pageNo);

}
