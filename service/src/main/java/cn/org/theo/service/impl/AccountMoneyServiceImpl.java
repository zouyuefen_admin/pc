package cn.org.theo.service.impl;

import cn.org.theo.mapper.AccountMoneyMapper;
import cn.org.theo.mapper.ConsultantMapper;
import cn.org.theo.mapper.ManagerMapper;
import cn.org.theo.mapper.UserMapper;
import cn.org.theo.pojo.*;
import cn.org.theo.service.AccountMoneyService;
import cn.org.theo.service.ManagerService;
import cn.org.theo.vo.AccountMoneyVo;
import cn.org.theo.vo.ConsultantFrontVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;

/**
 * project name: IntelliJ IDEA
 * Date: 2022/9/5 10:54
 *
 * @PackageName: cn.org.theo.service.impl
 * @ClassName: AccountMoneyServiceImpl
 * @Author: Theo
 * @Version:
 * @Description:
 */
@Service
public class AccountMoneyServiceImpl implements AccountMoneyService {

    @Autowired
    private UserMapper userMapper;
    @Autowired
    private AccountMoneyMapper accountMoneyMapper;
    @Autowired
    private ConsultantMapper consultantMapper;
    @Autowired
    private ManagerMapper managerMapper;
    @Override
    public Result getAccountList(String username,int pageNo) {
        HashMap<String,Object> map = new HashMap<>();
        if (username == null || "".equals(username)){
            return  Result.updateResp(0,"","请先登录",null);
        }
        User user = new User();
        user.setUsername(username);
        User query = userMapper.query(user);
        map.put("uid",query.getId());
        map.put("limit",(pageNo - 1) * 8);
        map.put("offset",8);
        List<AccountMoneyVo> list = accountMoneyMapper.getAccountMoneyList(map);
        Page<AccountMoneyVo> page = new Page();
        page.setPageNo(pageNo);
        page.setLimit((pageNo - 1) * 8);
        page.setOffset(8);
        page.setCondition(true);
        page.setList(list);
        page.setTotalCount(accountMoneyMapper.queryTotalCount(map));
        System.out.println(page.getTotalCount());
        int totalCount = page.getTotalCount();
        page.setTotalPageCount(totalCount % 8 == 0 ? totalCount / 8 : totalCount / 8 + 1);
        if (page.getTotalPageCount() == 0) {
            page.setCurrentPage(0);
        }
        return Result.updateResp(1,"数据获取成功","数据获取失败",page);
    }

    @Override
    public Result accountInfo(int id) {
        AccountMoneyVo accountMoneyVo = accountMoneyMapper.queryOne(id);
        if (accountMoneyVo.getRevenueExpense() == 1){
            accountMoneyVo.setRevenueExpenseName("支出");
        }else if (accountMoneyVo.getRevenueExpense() == 0){
            accountMoneyVo.setRevenueExpenseName("收入");
        }
        return new Result(0,"数据获取成功",accountMoneyVo);
    }

    @Override
    public Result queryAccountForBack(int pageNo, String username) {
        HashMap<String,Object> map = new HashMap<>();
        if (username == null || "".equals(username)){
            return  Result.updateResp(0,"","请先登录",null);
        }
        Manager manager = new Manager();
        manager.setUsername(username);
        Manager query = managerMapper.query(manager);
        Consultant consultant = new Consultant();
        consultant.setManagerId(query.getId());
        Consultant consultant1 = consultantMapper.query2(consultant);
        map.put("cid",consultant1.getId());
        map.put("limit",(pageNo - 1) * 5);
        map.put("offset",5);
        List<AccountMoneyVo> list = accountMoneyMapper.getAccountMoneyListForBack(map);
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getRevenueExpense() == 1){
                list.get(i).setRevenueExpenseName("支出");
            }else if (list.get(i).getRevenueExpense() == 0){
                list.get(i).setRevenueExpenseName("收入");
            }
        }
        Page<AccountMoneyVo> page = new Page();
        page.setPageNo(pageNo);
        page.setLimit((pageNo - 1) * 5);
        page.setOffset(5);
        page.setCondition(true);
        page.setList(list);
        page.setTotalCount(accountMoneyMapper.queryTotalCountForBack(map));
        System.out.println(page.getTotalCount());
        int totalCount = page.getTotalCount();
        page.setTotalPageCount(totalCount % 5 == 0 ? totalCount / 8 : totalCount / 5 + 1);
        if (page.getTotalPageCount() == 0) {
            page.setCurrentPage(0);
        }
        return Result.updateResp(1,"数据获取成功","数据获取失败",page);
    }
}
