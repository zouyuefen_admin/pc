package cn.org.theo.service;

import cn.org.theo.pojo.Consultant;
import cn.org.theo.pojo.Manager;
import cn.org.theo.pojo.Result;
import cn.org.theo.vo.ConsultantVo;

/**
 * project name: IntelliJ IDEA
 * Date: 2022/8/24 21:18
 *
 * @PackageName: cn.org.theo.service
 * @ClassName: ConsultantService
 * @Author: Theo
 * @Version:
 * @Description:
 */
public interface ConsultantService {

    Result showPositionTitle();

    Result query(ConsultantVo consultantVo,int pageNo);

    Result updateStatus(ConsultantVo consultantVo, String username);

    Result del(int id, String username);

    Result updatePwd(int id, String username);

    Result addAdmin(Consultant consultant);

    Result getFieldList();

    Result queryByFront(String fieldNameList, String consName, Integer pageNo);

    Result timeInfo(int consId);

    Result getConsInfoByIndex();
}
