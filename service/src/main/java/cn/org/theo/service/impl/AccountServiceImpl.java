package cn.org.theo.service.impl;

import cn.org.theo.mapper.UserMapper;
import cn.org.theo.pojo.Result;
import cn.org.theo.pojo.User;
import cn.org.theo.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * project name: IntelliJ IDEA
 * Date: 2022/9/2 16:27
 *
 * @PackageName: cn.org.theo.service.impl
 * @ClassName: AccountServiceImpl
 * @Author: Theo
 * @Version:
 * @Description:
 */
@Service
public class AccountServiceImpl implements AccountService {

    @Autowired
    private UserMapper userMapper;
    @Override
    public Result recharge(int money, String username) {

        User user = new User();
        user.setUsername(username);
        user.setBalance(money);
        int i = userMapper.updateBalance(user);

        return Result.updateResp(i,"充值成功！","充值失败！",null);
    }

    @Override
    public Result getAccountInfo(String username) {
        User user = new User();
        user.setUsername(username);
        User query = userMapper.query(user);
        int i = query == null ? 0: 1;
        return Result.updateResp(i,"获取余额成功","获取余额失败",query.getBalance());
    }

}
