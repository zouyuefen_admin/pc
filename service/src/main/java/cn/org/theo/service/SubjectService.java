package cn.org.theo.service;

import cn.org.theo.pojo.Result;
import cn.org.theo.vo.SubjectVo;

/**
 * project name: IntelliJ IDEA
 * Date: 2022/8/31 10:34
 *
 * @PackageName: cn.org.theo.service
 * @ClassName: SubjectService
 * @Author: Theo
 * @Version:
 * @Description:
 */
public interface SubjectService {
    Result getList(int pageNo, int fieldId);

    Result getOneSubject(int colNum, int fieldId);

    Result updateOneSubject(SubjectVo subjectVo);

    Result deleteOneSubject(SubjectVo subjectVo);

    Result addOneSubject(SubjectVo subjectVo);

    Result assessment(int score, int fieldId, String username);
}
