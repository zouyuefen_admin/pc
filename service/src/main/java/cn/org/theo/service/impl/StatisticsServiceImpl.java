package cn.org.theo.service.impl;

import cn.org.theo.mapper.SubscribeMapper;
import cn.org.theo.mapper.UserMapper;
import cn.org.theo.pojo.Result;
import cn.org.theo.qo.DateVo;
import cn.org.theo.qo.Month;
import cn.org.theo.qo.Week;
import cn.org.theo.qo.Year;
import cn.org.theo.service.StatisticsService;
import cn.org.theo.vo.WeekData;
import com.alibaba.fastjson.JSON;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * project name: IntelliJ IDEA
 * Date: 2022/9/8 18:14
 *
 * @PackageName: cn.org.theo.service.impl
 * @ClassName: StatisticsServiceImpl
 * @Author: Theo
 * @Version:
 * @Description:
 */
@Service
public class StatisticsServiceImpl implements StatisticsService {


    @Autowired
    private UserMapper userMapper;
    @Autowired
    private SubscribeMapper subscribeMapper;

    @Override
    public Result queryUserTotalForWeek(Week week) {
        List<String> list = new ArrayList<>();
        list.add(week.getW1());
        list.add(week.getW2());
        list.add(week.getW3());
        list.add(week.getW4());
        list.add(week.getW5());
        list.add(week.getW6());
        list.add(week.getW7());
        List<WeekData> weekDataList = userMapper.queryForWeekData(list);
        String jsonListStr1 = JSON.toJSONString(weekDataList);
        return new Result(0,"数据获取成功",jsonListStr1);
    }

    @Override
    public Result queryUserTotalForMonth(Month month) {
        List<Integer> list = new ArrayList<>();
        list.add(month.getM1());
        list.add(month.getM2());
        list.add(month.getM3());
        list.add(month.getM4());
        list.add(month.getM5());
        List<WeekData> weekDataList1 = new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {
            WeekData weekData = new WeekData();
            weekData.setDays(list.get(i) + "");
            weekDataList1.add(weekData);
        }
        List<WeekData> weekDataList = userMapper.queryForMonthData(list);
        for (WeekData weekData : weekDataList1) {
            for (WeekData w : weekDataList) {
                if (w.getDays().equals(weekData.getDays())){
                    weekData.setCount(w.getCount());
                }else if(w.getCount() == 0){
                    weekData.setCount(0);

                }
            }
        }
        String jsonListStr1 = JSON.toJSONString(weekDataList1);
        return new Result(0,"数据获取成功",jsonListStr1);
    }

    @Override
    public Result queryUserTotalForYear(Year year) {
        List<Integer> list = new ArrayList<>();
        list.add(year.getY1());
        list.add(year.getY2());
        list.add(year.getY3());
        list.add(year.getY4());
        list.add(year.getY5());
        list.add(year.getY6());
        List<WeekData> weekDataList1 = new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {
            WeekData weekData = new WeekData();
            if(list.get(i) >=1 && list.get(i) <=9){
                weekData.setDays("0" + list.get(i) + "");
            }
            weekDataList1.add(weekData);
        }
        weekDataList1.forEach(System.out::println);
        List<WeekData> weekDataList = userMapper.queryForYearData(list);
        weekDataList.forEach(System.out::println);
        for (WeekData weekData : weekDataList1) {
            for (WeekData w : weekDataList) {
                if (w.getDays().equals(weekData.getDays())){
                    weekData.setCount(w.getCount());
                }else if(w.getCount() == 0){
                    weekData.setCount(0);
                }
            }
        }
        weekDataList1.forEach(System.out::println);
        String jsonListStr1 = JSON.toJSONString(weekDataList1);
        return new Result(0,"数据获取成功",jsonListStr1);
    }

    @Override
    public Result querySubForData(DateVo dateVo) {
        List<WeekData> weekDataList = subscribeMapper.queryForStatistics(dateVo);
        String jsonListStr1 = JSON.toJSONString(weekDataList);
        return new Result(0,"数据获取成功",jsonListStr1);
    }
}
