package cn.org.theo.service.impl;

import cn.org.theo.mapper.ManagerMapper;
import cn.org.theo.mapper.ScheduleMapper;
import cn.org.theo.mapper.TimeSlotMapper;
import cn.org.theo.pojo.Manager;
import cn.org.theo.pojo.Result;
import cn.org.theo.pojo.Schedule;
import cn.org.theo.service.ScheduleService;
import cn.org.theo.vo.DateTimeVo;
import cn.org.theo.vo.ScheduleVo;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * project name: IntelliJ IDEA
 * Date: 2022/8/25 16:00
 *
 * @PackageName: cn.org.theo.service.impl
 * @ClassName: ScheduleServiceImpl
 * @Author: Theo
 * @Version:
 * @Description:
 */
@Service
public class ScheduleServiceImpl implements ScheduleService {
    @Autowired
    private ManagerMapper managerMapper;
    @Autowired
    private TimeSlotMapper timeSlotMapper;
    @Autowired
    private ScheduleMapper scheduleMapper;
    @Override
    public Result arrange(String list, String username) {
        List<DateTimeVo> dateTimeList = JSON.parseArray(list, DateTimeVo.class);
        dateTimeList.forEach(System.out::println);
        Manager manager = new Manager();
        manager.setUsername(username);
        Manager query = managerMapper.query(manager);

        List<Schedule> scheduleList = new ArrayList<>();

        for (int i = 0; i < dateTimeList.size(); i++) {
            //切割时间
            String[] s = dateTimeList.get(i).getDateTime().split(" ");
            String dateTime = s[0];
            String time = s[1] + ":00";
            Schedule schedule = new Schedule();
            schedule.setDateTime(dateTime);
            schedule.setTsId(timeSlotMapper.query(time));
            schedule.setManagerId(query.getId());
            schedule.setEnable(1);
            scheduleList.add(schedule);
        }
        for (Schedule schedule : scheduleList) {
            scheduleMapper.insert(schedule);
        }
        return Result.updateResp(1,"插入成功","",null);
    }

    @Override
    public Result getDateInfo(String username) {
        Manager manager = new Manager();
        manager.setUsername(username);
        Manager query = managerMapper.query(manager);

        //根据mid查询排班
        List<ScheduleVo> scheduleList = scheduleMapper.query(query);

        List<DateTimeVo> dateTimeList = new ArrayList<>();

        for (ScheduleVo scheduleVo : scheduleList) {
            DateTimeVo dateTimeVo = new DateTimeVo();
            String[] s = scheduleVo.getDateTime().split(" ");
            String date = s[0];
            String slot = scheduleVo.getSlot();
            String[] split = slot.split(":");
            String time = split[0];
            String dateTime = date + "-" + time;
            dateTimeVo.setDateTime(dateTime);
            dateTimeList.add(dateTimeVo);
        }

        String s = JSON.toJSONString(dateTimeList);
        System.out.println(s);
        return Result.updateResp(1, "数据获取成功","",s);
    }
}
