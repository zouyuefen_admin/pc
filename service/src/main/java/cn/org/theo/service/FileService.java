package cn.org.theo.service;

import cn.org.theo.pojo.Result;

/**
 * project name: IntelliJ IDEA
 * Date: 2022/9/11 22:02
 *
 * @PackageName: cn.org.theo.service
 * @ClassName: FileService
 * @Author: Theo
 * @Version:
 * @Description:
 */
public interface FileService {
    Result dealExcel(String path);
}
