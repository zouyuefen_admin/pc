package cn.org.theo.service.impl;

import cn.org.theo.mapper.JournalMapper;
import cn.org.theo.mapper.ManagerMapper;
import cn.org.theo.mapper.MenuConsRelMapper;
import cn.org.theo.mapper.MenuMapper;
import cn.org.theo.pojo.*;
import cn.org.theo.service.ManagerService;
import cn.org.theo.vo.MenuVo;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * project name: IntelliJ IDEA
 * Date: 2022/8/23 21:40
 *
 * @PackageName: cn.org.theo.service.impl
 * @ClassName: ManagerServiceImpl
 * @Author: Theo
 * @Version:
 * @Description:
 */
@Service
public class ManagerServiceImpl implements ManagerService {

    @Autowired
    private ManagerMapper managerMapper;

    @Autowired
    private MenuConsRelMapper menuConsRelMapper;
    @Autowired
    private MenuMapper menuMapper;

    @Autowired
    private JournalMapper journalMapper;
    @Override
    public int login(Manager manager) {
        Manager manager1 = managerMapper.query(manager);
        if (manager1 != null){
            if (manager1.getStatus() == 3){
                System.out.println(manager1);
                return 2;
            }
            return 1;
        }else{

            return 0;
        }
    }

    @Override
    public Result getAdminInfo(String username) {

        Map<String, Object> map = new HashMap<>();
        //判断用户是管理员还是咨询师
        Manager manager = new Manager();
        manager.setUsername(username);
        Manager query = managerMapper.query(manager);
        Integer role = query.getRole();
        int authority = 0;
        if (role == 1){
            authority = 1;
        }

        List<Menu> menuList = menuMapper.getList(authority);
        menuList.forEach(System.out::println);

        HashMap<String ,List<String>> map1 = new HashMap<>();

        List<Menu> parentList = menuMapper.getList(-1);

        for (Menu menu : parentList) {
            Integer id = menu.getId();
            List<String> list = new ArrayList<>();
            for (Menu menu1 : menuList) {
                if (menu1.getMid() == id){
                    list.add(menu1.getName() + "-" + menu1.getHref());
                }
            }
            map1.put(menu.getName(),list);
        }
        for (Map.Entry<String, List<String>> entry : map1.entrySet()) {
            System.out.println("key = " + entry.getKey() + ", value = " + entry.getValue());
        }
        JSONObject itemJSONObj = JSONObject.parseObject(JSON.toJSONString(map1));
        System.out.println(itemJSONObj.toString());

        map.put("username", username);
        map.put("role",role);
        return Result.updateResp(1, itemJSONObj.toString(), "", map);
    }


}
