package cn.org.theo.service.impl;

import cn.org.theo.mapper.ConsultantMapper;
import cn.org.theo.mapper.SubscribeMapper;
import cn.org.theo.mapper.UserMapper;
import cn.org.theo.pojo.*;
import cn.org.theo.qo.SubscribeForBackQo;
import cn.org.theo.service.SubscribeService;
import cn.org.theo.vo.ConsultantFrontVo;
import cn.org.theo.vo.ConsultantVo;
import cn.org.theo.vo.SubscribeForBackVo;
import cn.org.theo.vo.SubscribeVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * project name: IntelliJ IDEA
 * Date: 2022/8/30 18:54
 *
 * @PackageName: cn.org.theo.service.impl
 * @ClassName: SubscribeServiceImpl
 * @Author: Theo
 * @Version:
 * @Description:
 */
@Service
public class SubscribeServiceImpl implements SubscribeService {

    @Autowired
    private UserMapper userMapper;
    @Autowired
    private ConsultantMapper consultantMapper;
    @Autowired
    private SubscribeMapper subscribeMapper;

    @Override
    public Result getSubList(SubscribeForBackQo s,int pageNo) {
        System.out.println(s);
        User query = new User();
        System.out.println(s.getUsername());
        System.out.println(s.getUsername() == null);
        if (s.getUsername() != null && !"".equals(s.getUsername())){
            User user = new User();
            user.setUsername(s.getUsername());
            query = userMapper.query(user);
            System.out.println(query);
        }
        Consultant consultant = new Consultant();
        if (s.getConsName() != null && !"".equals(s.getConsName())){
            Consultant user = new Consultant();
            user.setName(s.getConsName());
            consultant = consultantMapper.query4(user);
            System.out.println(consultant);
        }
        //已有咨询师名字，用户名，预约时间
        //获取咨询师managerId,用户uid
        //需要获取预约时间，咨询师姓名，领域，用户名，完成情况，状态


        Map<String,Object> map = new HashMap<>();
        //通过manager的name获取manager_id，放入map
        map.put("managerId",consultant.getManagerId());
        map.put("userId",query.getId());

        //将查询条件放入map，3个
        if (s.getStime() != null && !"".equals(s.getStime())){
            map.put("startTime",s.getStime().replace('T',' '));
        }else{
            map.put("startTime","1999-08-29 00:00");
        }
        if (s.getEtime() != null && !"".equals(s.getEtime())){
            map.put("endTime",s.getEtime().replace('T',' '));
        }else{
            map.put("endTime",new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date(System.currentTimeMillis())));
        }


        map.put("limit",(pageNo - 1) * 5);
        map.put("offset",5);

        List<SubscribeForBackVo> subscribeForBackVo = subscribeMapper.queryForConditionByBackPage(map);
        for (SubscribeForBackVo forBackVo : subscribeForBackVo) {
            SubscribeForBackVo subscribeForBackVo1 = subscribeMapper.queryOne2(forBackVo);
            if (subscribeForBackVo1 != null){
                forBackVo.setFinishTime(subscribeForBackVo1.getFinishTime());
            }else{
                forBackVo.setFinishTime(""+0);
            }
        }
        Page<SubscribeForBackVo> page = new Page();
        page.setPageNo(pageNo);
        page.setLimit((pageNo - 1) * 5);
        page.setOffset(5);
        page.setCondition(true);
        page.setList(subscribeForBackVo);
        page.setTotalCount(subscribeMapper.getTotalCountBackPage(map));
        System.out.println(subscribeMapper.getTotalCountBackPage(map));
        int totalCount = subscribeMapper.getTotalCountBackPage(map);
        page.setTotalPageCount(totalCount % 5 == 0 ? totalCount / 5 : totalCount / 5 + 1);
        if (page.getTotalPageCount() == 0) {
            page.setCurrentPage(0);
        }
        return Result.updateResp(1,"数据获取成功","数据获取失败",page);
    }

    @Override
    public Result ban(int id) {
        int update = subscribeMapper.update3(id);
        return Result.updateResp(update,"预约停止成功！","预约停止失败！","");
    }
}
