package cn.org.theo.service;

import cn.org.theo.pojo.Result;
import cn.org.theo.pojo.Subscribe;
import cn.org.theo.qo.OrderListQo;
import cn.org.theo.qo.SubscribeFrontQo;
import cn.org.theo.qo.SubscribeQo;

/**
 * project name: IntelliJ IDEA
 * Date: 2022/8/26 9:59
 *
 * @PackageName: cn.org.theo.service
 * @ClassName: OrderService
 * @Author: Theo
 * @Version:
 * @Description:
 */
public interface OrderService {
    public Result getOrderStatusList();

    Result getListForPage(SubscribeQo subscribeQo,int pageNo);

    Result createOrder(SubscribeFrontQo subscribeFrontQo);

    Result showOrder(String username, int pageNo);

    Result updateStatus(int id);

    Result updateStatusForDiagnosis(int id, String answer);

    Result getConsInfo(String consName);

    Result evaluate(String orderId, String evaluate);

    Result subscribeInfoForOne(int id);

    Result haveMoney(String username, int consId);

    Result haveMoneyForOnlineExam(String username);
}
