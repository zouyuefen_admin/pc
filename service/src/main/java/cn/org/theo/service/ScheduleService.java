package cn.org.theo.service;

import cn.org.theo.pojo.Result;

/**
 * project name: IntelliJ IDEA
 * Date: 2022/8/25 15:59
 *
 * @PackageName: cn.org.theo.service
 * @ClassName: ScheduleService
 * @Author: Theo
 * @Version:
 * @Description:
 */
public interface ScheduleService {

    Result arrange(String list,String username);

    Result getDateInfo(String username);
}
