package cn.org.theo.service;

import cn.org.theo.pojo.Result;
import cn.org.theo.qo.SubscribeForBackQo;

/**
 * project name: IntelliJ IDEA
 * Date: 2022/8/30 18:54
 *
 * @PackageName: cn.org.theo.service
 * @ClassName: SubscribeService
 * @Author: Theo
 * @Version:
 * @Description:
 */
public interface SubscribeService {
    Result getSubList(SubscribeForBackQo subscribeForBackQo,int pageNo);

    Result ban(int id);
}
