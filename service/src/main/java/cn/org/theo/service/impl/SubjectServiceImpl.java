package cn.org.theo.service.impl;

import cn.org.theo.mapper.*;
import cn.org.theo.pojo.*;
import cn.org.theo.service.SubjectService;
import cn.org.theo.vo.SubjectVo;
import cn.org.theo.vo.SubscribeForBackVo;
import org.apache.ibatis.logging.stdout.StdOutImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * project name: IntelliJ IDEA
 * Date: 2022/8/31 10:34
 *
 * @PackageName: cn.org.theo.service.impl
 * @ClassName: SubjectServiceImpl
 * @Author: Theo
 * @Version:
 * @Description:
 */
@Service
public class SubjectServiceImpl implements SubjectService {

    @Autowired
    private SubjectMapper subjectMapper;
    @Autowired
    private FieldMapper fieldMapper;
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private AssessmentMapper assessmentMapper;
    @Autowired
    private ConfigureMapper configureMapper;
    @Autowired
    private AccountMoneyMapper accountMoneyMapper;


    @Override
    public Result getList(int pageNo, int fieldId) {
        Map<String,Object> map = new HashMap<>();

        map.put("fieldId",fieldId);
        map.put("limit",(pageNo - 1) * 5);
        map.put("offset",5);
        List<Subject> subjects = subjectMapper.queryBackPage(map);

        Page<Subject> page = new Page();
        page.setPageNo(pageNo);
        page.setLimit((pageNo - 1) * 5);
        page.setOffset(5);
        page.setCondition(true);
        page.setList(subjects);
        page.setTotalCount(subjectMapper.getTotalCount(map));
        int totalCount = subjectMapper.getTotalCount(map);
        page.setTotalPageCount(totalCount % 5 == 0 ? totalCount / 5 : totalCount / 5 + 1);
        if (page.getTotalPageCount() == 0) {
            page.setCurrentPage(0);
        }
        return Result.updateResp(1,"数据获取成功","数据获取失败",page);

    }

    @Override
    public Result getOneSubject(int colNum, int fieldId) {

        Map<String,Object> map = new HashMap<>();

        map.put("colNum",colNum);
        map.put("fieldId",fieldId);
        SubjectVo subject = subjectMapper.queryOne(map);

        int i = subject == null ? 0 : 1;

        return Result.updateResp(i,"试题修改数据获取成功","试题修改数据获取失败",subject);
    }

    @Override
    public Result updateOneSubject(SubjectVo subjectVo) {
        System.out.println(subjectVo);
        int fieidId = fieldMapper.queryId2(subjectVo.getFieldName());
        subjectVo.setFieldId(fieidId);
        int i = subjectMapper.update(subjectVo);
        return Result.updateResp(i,"修改成功","修改失败",null);
    }

    @Override
    public Result deleteOneSubject(SubjectVo subjectVo) {
        int i = subjectMapper.delete(subjectVo);
        return Result.updateResp(i,"删除成功","删除失败",null);
    }

    @Override
    public Result addOneSubject(SubjectVo subjectVo) {
        int count = subjectMapper.queryTotalCount(subjectVo.getFieldId());
        Integer maxColId;
        if (count != 0){
            maxColId = subjectMapper.queryMaxColId(subjectVo.getFieldId());
            maxColId++;
            subjectVo.setColNum(maxColId);
        }else{
            maxColId = 1;
        }
        subjectVo.setColNum(maxColId);
        int i = subjectMapper.add(subjectVo);
        return Result.updateResp(i,"添加成功","添加失败",null);
    }

    @Override
    public Result assessment(int score, int fieldId, String username) {

        User user = new User();
        user.setUsername(username);

        User query = userMapper.query(user);
        System.err.println(query);
        //判断余额是否足够
        if(query.getBalance() < 100){
            System.out.println(66666);
            return new Result(-1,"用户余额不足，请及时充值",null);
        }

        Assessment assessment = new Assessment();
        String asseInfo;
        if (score <= 100 && score >= 90){
            asseInfo = "心理情况健康，请继续保持";
        }else if (score < 90 && score >= 80){
            asseInfo = "心理情况良好，请放轻松！";
        }else if (score < 80 && score >= 70){
            asseInfo = "中等抑郁，中等焦虑";
        }else if (score < 70 && score >= 60){
            asseInfo = "心理情况有点不好，请抓紧治疗";
        }else {
            asseInfo = "极大可能患有抑郁症！";
        }
        assessment.setAsseInfo("得分：" + score);
        assessment.setAsseResult(asseInfo);
        assessment.setUId(query.getId());
        assessment.setScore(score);
        assessment.setAsseTime(new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date(System.currentTimeMillis())));
        assessment.setFieldId(fieldId);
        int i = assessmentMapper.insert(assessment);
        //用户扣去对应服务费用
        Configure configure = new Configure();
        configure.setKeyp("exam_cost");
        Configure configure1 = configureMapper.queryForOne(configure);
        HashMap<String,Object> map = new HashMap<>();
        map.put("servicePrice",configure1.getValue());
        map.put("id",query.getId());
        int i2  = userMapper.costMoney(map);

        //插入account_money表
        AccountMoney accountMoney = new AccountMoney();
        accountMoney.setMoney(Integer.parseInt(configure1.getValue()));
        accountMoney.setRevenueExpense(1);//0收入/1支出
        accountMoney.setRelationship("系统");
        accountMoney.setMatterId(1);
        accountMoney.setUId(query.getId());
        accountMoney.setCreateTime(new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date(System.currentTimeMillis())));
        int i4 = accountMoneyMapper.insert(accountMoney);
        return Result.updateResp(i4,"插入成功","插入失败",null);
    }
}
