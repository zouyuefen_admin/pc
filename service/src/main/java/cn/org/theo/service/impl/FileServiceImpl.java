package cn.org.theo.service.impl;

import cn.org.theo.mapper.FieldMapper;
import cn.org.theo.mapper.SubjectMapper;
import cn.org.theo.pojo.DemoData;
import cn.org.theo.pojo.Result;
import cn.org.theo.service.FileService;
import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.ExcelReader;
import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;
import com.alibaba.excel.read.metadata.ReadSheet;
import com.alibaba.excel.read.metadata.holder.ReadSheetHolder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * project name: IntelliJ IDEA
 * Date: 2022/9/11 22:03
 *
 * @PackageName: cn.org.theo.service.impl
 * @ClassName: FileServiceImpl
 * @Author: Theo
 * @Version:
 * @Description:
 */
@Service
public class FileServiceImpl implements FileService {

    @Autowired
    private FieldMapper fieldMapper;
    @Autowired
    private SubjectMapper subjectMapper;

    @Override
    public Result dealExcel(String path) {
        System.out.println("aaaa:" + path);
        List<DemoData> list = new ArrayList<>();
        // 创建一个数据格式来装读取到的数据
        Class<DemoData> head = DemoData.class;
        // 创建ExcelReader对象
        ExcelReader excelReader = EasyExcel.read(path, head, new AnalysisEventListener<DemoData>() {
            // 每解析一行数据,该方法会被调用一次
            @Override
            public void invoke(DemoData demoData, AnalysisContext analysisContext) {
                list.add(demoData);
            }
            // 全部解析完成被调用
            @Override
            public void doAfterAllAnalysed(AnalysisContext analysisContext) {
                System.out.println("解析完成...");
                // 可以将解析的数据保存到数据库
                ReadSheetHolder readSheetHolder = analysisContext.readSheetHolder();
                String fieldName = readSheetHolder.getSheetName();
                System.out.println(fieldName);

                int i = fieldMapper.queryId2(fieldName);
                if (i == 0){
                    return;
                }
                int count = subjectMapper.queryTotalCount(i);
                Integer maxColId;
                if (count != 0){
                    maxColId = subjectMapper.queryMaxColId(i);
                }else{
                    maxColId = 1;
                }
                for (int i1 = 0; i1 < list.size(); i1++) {
                    list.get(i1).setColNum(maxColId);
                    maxColId++;
                    list.get(i1).setFieldId(i);
                }
                subjectMapper.addList(list);
                list.forEach(System.out::println);
            }
        }).build();
        // 创建sheet对象,并读取Excel的第一个sheet(下标从0开始), 也可以根据sheet名称获取
        ReadSheet sheet = EasyExcel.readSheet(0).build();
        // 读取sheet表格数据, 参数是可变参数,可以读取多个sheet
        excelReader.read(sheet);
        // 读取sheet表格数据, 参数是可变参数,可以读取多个sheet
        excelReader.finish();

        return new Result(0,"批量插入成功！","");
    }
}
