package cn.org.theo.service;

import cn.org.theo.pojo.Menu;
import cn.org.theo.pojo.Result;
import cn.org.theo.qo.MenuQo;

/**
 * project name: IntelliJ IDEA
 * Date: 2022/9/5 16:41
 *
 * @PackageName: cn.org.theo.service
 * @ClassName: MenuService
 * @Author: Theo
 * @Version:
 * @Description:
 */
public interface MenuService {


    Result getMenuList();

    Result getMenuListForCondition(MenuQo menuQo);

    Result add(MenuQo menuQo);

    Result getOneMenu(int id);

    Result update(MenuQo menuQo);

    Result delete(MenuQo menuQo);

    Result getManagerList(int authority);

    Result getManagerList2(int authority);

    Result singleRight(Menu menu);

    Result singleLeft(Menu menu);

    Result doubleLeft(Menu menu);


    Result doubleRight(Menu menu);
}
