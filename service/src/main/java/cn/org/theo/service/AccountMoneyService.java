package cn.org.theo.service;

import cn.org.theo.pojo.Result;

/**
 * project name: IntelliJ IDEA
 * Date: 2022/9/5 10:54
 *
 * @PackageName: cn.org.theo.service
 * @ClassName: AccountMoneyService
 * @Author: Theo
 * @Version:
 * @Description:
 */
public interface AccountMoneyService {
    Result getAccountList(String username,int pageNo);

    Result accountInfo(int id);

    Result queryAccountForBack(int pageNo, String username);
}
