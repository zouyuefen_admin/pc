package cn.org.theo.service;

import cn.org.theo.pojo.Result;

/**
 * project name: IntelliJ IDEA
 * Date: 2022/9/2 16:26
 *
 * @PackageName: cn.org.theo.service
 * @ClassName: AccountService
 * @Author: Theo
 * @Version:
 * @Description:
 */
public interface AccountService {
    Result recharge(int money, String username);

    Result getAccountInfo(String username);

}
