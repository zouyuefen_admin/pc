package cn.org.theo.service;

import cn.org.theo.pojo.Result;
import cn.org.theo.qo.AssessmentQo;

/**
 * project name: IntelliJ IDEA
 * Date: 2022/9/1 16:26
 *
 * @PackageName: cn.org.theo.service
 * @ClassName: AssessmentService
 * @Author: Theo
 * @Version:
 * @Description:
 */
public interface AssessmentService {
    Result queryForCondition(AssessmentQo assessmentQo, int pageNo);

    Result assessmentForOne(int id);


    Result queryForConditionByFront(int pageNo, String username);

}
