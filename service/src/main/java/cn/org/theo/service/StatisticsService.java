package cn.org.theo.service;

import cn.org.theo.pojo.Result;
import cn.org.theo.qo.DateVo;
import cn.org.theo.qo.Month;
import cn.org.theo.qo.Week;
import cn.org.theo.qo.Year;

/**
 * project name: IntelliJ IDEA
 * Date: 2022/9/8 18:14
 *
 * @PackageName: cn.org.theo.service
 * @ClassName: StatisticsService
 * @Author: Theo
 * @Version:
 * @Description:
 */
public interface StatisticsService {
    Result queryUserTotalForWeek(Week week);

    Result queryUserTotalForMonth(Month month);

    Result queryUserTotalForYear(Year year);

    Result querySubForData(DateVo dateVo);
}
