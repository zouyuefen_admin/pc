package cn.org.theo.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * project name: IntelliJ IDEA
 * Date: 2022/8/20 19:54
 *
 * @PackageName: cn.org.theo.pojo
 * @ClassName: ConsultantField
 * @Author: Theo
 * @Version:
 * @Description: 咨询师,咨询领域关联表
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ConsultantField {

    private int id;

    private int consId;

    private int fieldId;

}
