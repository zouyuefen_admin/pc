package cn.org.theo.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * project name: IntelliJ IDEA
 * Date: 2022/8/20 19:54
 *
 * @PackageName: cn.org.theo.pojo
 * @ClassName: Consultant
 * @Author: Theo
 * @Version:
 * @Description: 咨询师
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Consultant {

    private Integer id;

    private String name;

    private String school;

    private String positionalTitles;

    private String professionalBackground;

    private String headImg;

    private Integer servicePrice;

    private String introduction;

    private Integer managerId;
}
