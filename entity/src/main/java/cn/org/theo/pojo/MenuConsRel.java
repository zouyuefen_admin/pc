package cn.org.theo.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * project name: IntelliJ IDEA
 * Date: 2022/8/23 15:32
 *
 * @PackageName: cn.org.theo.pojo
 * @ClassName: MenuConsRel
 * @Author: Theo
 * @Version:
 * @Description:
 */
@AllArgsConstructor
@Data
@NoArgsConstructor
public class MenuConsRel {
    private Integer id;
    private Integer menuId;
    private Integer status;
}
