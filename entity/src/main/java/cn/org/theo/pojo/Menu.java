package cn.org.theo.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * project name: IntelliJ IDEA
 * Date: 2022/8/23 15:35
 *
 * @PackageName: cn.org.theo.pojo
 * @ClassName: Menu
 * @Author: Theo
 * @Version:
 * @Description:
 */
@Data
@NoArgsConstructor
@AllArgsConstructor

public class Menu {

    private Integer id;
    private String name;
    private String href;
    private Integer mid;
    private Integer isParent;
    private Integer authority;

}
