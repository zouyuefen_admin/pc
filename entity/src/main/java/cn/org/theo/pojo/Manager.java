package cn.org.theo.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * project name: IntelliJ IDEA
 * Date: 2022/8/23 20:59
 *
 * @PackageName: cn.org.theo.pojo
 * @ClassName: Manager
 * @Author: Theo
 * @Version:
 * @Description:
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
public class Manager {
    private Integer id;
    private String username;
    private String password;
    private Integer status;
    private Integer role;
}
