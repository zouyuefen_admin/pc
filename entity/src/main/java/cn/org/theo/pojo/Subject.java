package cn.org.theo.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * project name: IntelliJ IDEA
 * Date: 2022/8/23 15:13
 *
 * @PackageName: cn.org.theo.pojo
 * @ClassName: Subject
 * @Author: Theo
 * @Version:
 * @Description:
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
public class Subject {

    private int id;
    private String name;
    private String option1;
    private String option2;
    private String option3;
    private String option4;
    private Integer fieldId;
    private Integer colNum;


}
