package cn.org.theo.pojo;

import java.util.ArrayList;
import java.util.List;

public class Page<T> {

    private int limit;

    private int currentPage = 1;

    private int pageNo = 1;

    private int offset;
    private int totalCount;

    private int totalPageCount;

    private boolean isCondition;

    private List<T> list = new ArrayList<>();

    public boolean isCondition() {
        return isCondition;
    }

    public void setCondition(boolean condition) {
        isCondition = condition;
    }

    @Override
    public String toString() {
        return "Page{" +
                "limit=" + limit +
                ", currentPage=" + currentPage +
                ", pageNo=" + pageNo +
                ", offset=" + offset +
                ", totalCount=" + totalCount +
                ", totalPageCount=" + totalPageCount +
                ", isCondition=" + isCondition +
                ", list=" + list +
                '}';
    }

    public int getPageNo() {
        return pageNo;
    }

    public void setPageNo(int pageNo) {
        this.pageNo = pageNo;
    }

    public int getTotalPageCount() {
        return totalPageCount;
    }

    public void setTotalPageCount(int totalPageCount) {
        this.totalPageCount = totalPageCount;
    }

    public int getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public int getOffset() {
        return offset;
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }

    public int getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }

    public List<T> getList() {
        return list;
    }

    public void setList(List<T> list) {
        this.list = list;
    }
}
