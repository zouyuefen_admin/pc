package cn.org.theo.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
/**
 * project name: IntelliJ IDEA
 * Date: 2022/8/20 19:54
 *
 * @PackageName: cn.org.theo.pojo
 * @ClassName: Field
 * @Author: Theo
 * @Version:
 * @Description: 咨询领域
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Field {

    private int id;

    private String fieldName;

}
