package cn.org.theo.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


/**
 * project name: IntelliJ IDEA
 * Date: 2022/8/20 19:54
 *
 * @PackageName: cn.org.theo.pojo
 * @ClassName: OrderStatus
 * @Author: Theo
 * @Version:
 * @Description: 订单状态表
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class OrderStatus {
    private Integer id;
    private String status;
}
