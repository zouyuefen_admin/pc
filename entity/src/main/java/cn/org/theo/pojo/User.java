package cn.org.theo.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
/**
 * project name: IntelliJ IDEA
 * Date: 2022/8/20 19:54
 *
 * @PackageName: cn.org.theo.pojo
 * @ClassName: User
 * @Author: Theo
 * @Version:
 * @Description: 用户表
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class User {
    private Integer id;

    private String username;

 
    private String password;

   
    private String sex;


    private Integer age;


    private String phone;


    private String address;


    private Integer balance;

    private String regTime;

    private Integer status;

}
