package cn.org.theo.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * project name: IntelliJ IDEA
 * Date: 2022/8/23 15:30
 *
 * @PackageName: cn.org.theo.pojo
 * @ClassName: Journal
 * @Author: Theo
 * @Version:
 * @Description:
 */
@NoArgsConstructor
@Data
@AllArgsConstructor
public class Journal {

    private Integer id;
    private Integer userId;
    private String time;
    private String option;

}
