package cn.org.theo.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Theo
 * @create 2022-06-11 12:39
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Result {
	private int code; //0成功 ，-1失败
	private String msg;
	private Object data;

    public static Result updateResp(int i, String succes, String fail, Object o) {
		return new Result(i == 1 ? 0:1,i == 1 ? succes:fail,o);
	}
}
