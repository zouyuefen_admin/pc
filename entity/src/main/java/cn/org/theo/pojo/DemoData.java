package cn.org.theo.pojo;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.*;

/**
 * project name: IntelliJ IDEA
 * Date: 2022/9/11 22:55
 *
 * @PackageName: cn.org.theo.pojo
 * @ClassName: DemoData
 * @Author: Theo
 * @Version:
 * @Description:
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class DemoData {
    @ExcelProperty("题目名")
    private String name;
    /**
     * 用名字去匹配，这里需要注意，如果名字重复，会导致只有一个字段读取到数据
     * 强制读取第三个 这里不建议 index 和 name 同时用，要么一个对象只用index，要么一个对象只用name去匹配
     */
    @ExcelProperty("选项一")
    private String option1;

    @ExcelProperty("选项二")
    private String option2;

    @ExcelProperty("选项三")
    private String option3;

    @ExcelProperty("选项四")
    private String option4;
    private Integer fieldId;
    private Integer colNum;

}
