package cn.org.theo.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * project name: IntelliJ IDEA
 * Date: 2022/8/23 15:44
 *
 * @PackageName: cn.org.theo.pojo
 * @ClassName: TimeSlot
 * @Author: Theo
 * @Version:
 * @Description:
 */
@Data
@NoArgsConstructor
@AllArgsConstructor

public class TimeSlot {

    private Integer id;
    private String slot;

}
