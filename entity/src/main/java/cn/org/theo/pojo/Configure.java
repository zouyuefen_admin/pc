package cn.org.theo.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * project name: IntelliJ IDEA
 * Date: 2022/8/23 15:11
 *
 * @PackageName: cn.org.theo.pojo
 * @ClassName: Configure
 * @Author: Theo
 * @Version:
 * @Description:
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Configure {

    private Integer id;
    private String keyp;
    private String value;

}
