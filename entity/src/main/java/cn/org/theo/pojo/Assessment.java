package cn.org.theo.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * project name: IntelliJ IDEA
 * Date: 2022/8/20 19:54
 *
 * @PackageName: cn.org.theo.pojo
 * @ClassName: Assessment
 * @Author: Theo
 * @Version:
 * @Description: 评估报告表
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Assessment {

    private int id;

    private int uId;

    private String asseResult;

    private String asseInfo;

    private Integer score;
    private String asseTime;
    private Integer fieldId;


}
