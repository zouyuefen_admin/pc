package cn.org.theo.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * project name: IntelliJ IDEA
 * Date: 2022/8/20 19:54
 *
 * @PackageName: cn.org.theo.pojo
 * @ClassName: AccountMoney
 * @Author: Theo
 * @Version:
 * @Description: 账户资金往来记录表
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AccountMoney {

    private int id;

    private int cId;

    private int uId;

    private Integer matterId;

    private String relationship;
    private String createTime;

    private int revenueExpense;

    private int money;

}
