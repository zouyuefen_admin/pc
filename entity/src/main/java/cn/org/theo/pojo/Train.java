package cn.org.theo.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * project name: IntelliJ IDEA
 * Date: 2022/8/23 15:40
 *
 * @PackageName: cn.org.theo.pojo
 * @ClassName: Train
 * @Author: Theo
 * @Version:
 * @Description:
 */
@Data
@NoArgsConstructor
@AllArgsConstructor

public class Train {

    private Integer id;
    private String name;
    private Integer fieldId;
    private Integer num;
    private Integer price;
    private String startTime;
    private String endTime;
    private String introduce;

}
