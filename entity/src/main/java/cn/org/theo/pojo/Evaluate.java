package cn.org.theo.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * project name: IntelliJ IDEA
 * Date: 2022/8/20 19:54
 *
 * @PackageName: cn.org.theo.pojo
 * @ClassName: Evaluate
 * @Author: Theo
 * @Version:
 * @Description: 用户评价表
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Evaluate {

    private int id;

    private String subId;

    private String evaluateInfo;
    private String evaluateTime;
}
