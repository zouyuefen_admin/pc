package cn.org.theo.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * project name: IntelliJ IDEA
 * Date: 2022/8/20 19:54
 *
 * @PackageName: cn.org.theo.pojo
 * @ClassName: Subscribe
 * @Author: Theo
 * @Version:
 * @Description: 预约记录表
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Subscribe {

    private int id;

    private int uId;

    private int managerId;

    private int subStatus;

    private String questionInfo;

    private String subTime;

    private String answerTime;

    private String subId;

    private Integer fieldId;

    private String answer;
}
