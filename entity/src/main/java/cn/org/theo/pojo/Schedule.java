package cn.org.theo.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * project name: IntelliJ IDEA
 * Date: 2022/8/23 15:49
 *
 * @PackageName: cn.org.theo.pojo
 * @ClassName: Schedule
 * @Author: Theo
 * @Version:
 * @Description:
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
public class Schedule {
    private Integer id;
    private Integer tsId;
    private String dateTime;
    private Integer managerId;
    private Integer enable;

}
