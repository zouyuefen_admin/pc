package cn.org.theo.pojo;

import lombok.Data;

/**
 * project name: IntelliJ IDEA
 * Date: 2022/9/1 17:04
 *
 * @PackageName: cn.org.theo.pojo
 * @ClassName: AssessmentVo
 * @Author: Theo
 * @Version:
 * @Description:
 */
@Data
public class AssessmentVo {
    private int id;

    private int uId;
    private String username;
    private String fieldName;

    private String asseResult;

    private String asseInfo;

    private Integer score;
    private String asseTime;
    private Integer fieldId;


}
