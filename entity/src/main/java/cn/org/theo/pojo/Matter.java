package cn.org.theo.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
/**
 * project name: IntelliJ IDEA
 * Date: 2022/8/20 19:54
 *
 * @PackageName: cn.org.theo.pojo
 * @ClassName: Matter
 * @Author: Theo
 * @Version:
 * @Description: 事项表
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Matter {

    private int id;

    private String matterName;

}
