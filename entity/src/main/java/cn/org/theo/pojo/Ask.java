package cn.org.theo.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * project name: IntelliJ IDEA
 * Date: 2022/8/23 15:37
 *
 * @PackageName: cn.org.theo.pojo
 * @ClassName: Ask
 * @Author: Theo
 * @Version:
 * @Description:
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
public class Ask {

    private Integer id;
    private Integer tId;
    private Integer consId;
}
