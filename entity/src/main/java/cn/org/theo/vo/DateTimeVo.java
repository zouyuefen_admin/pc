package cn.org.theo.vo;

import lombok.Data;

/**
 * project name: IntelliJ IDEA
 * Date: 2022/8/25 22:48
 *
 * @PackageName: cn.org.theo.vo
 * @ClassName: DateTimeVo
 * @Author: Theo
 * @Version:
 * @Description:
 */
@Data
public class DateTimeVo {

    private String dateTime;

}
