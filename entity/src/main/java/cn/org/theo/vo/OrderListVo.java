package cn.org.theo.vo;

import lombok.Data;

/**
 * project name: IntelliJ IDEA
 * Date: 2022/8/29 18:10
 *
 * @PackageName: cn.org.theo.vo
 * @ClassName: OrderListVo
 * @Author: Theo
 * @Version:
 * @Description:
 */
@Data
public class OrderListVo {

    private String orderId;
    private String username;
    private String fieldName;
    private String subTime;
    private String subStatus;

    private Integer cost;
    private Integer managerId;

    private String consName;

    private String questionInfo;
    private String answer;
    private String answerTime;

    private String evaluate;

}
