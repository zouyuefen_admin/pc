package cn.org.theo.vo;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * project name: IntelliJ IDEA
 * Date: 2022/8/27 10:48
 *
 * @PackageName: cn.org.theo.vo
 * @ClassName: ConsultantFrontVo
 * @Author: Theo
 * @Version:
 * @Description:
 */
@Data
public class ConsultantFrontVo {

    private int id;

    private String name;

    private String school;

    private String positionalTitles;

    private String professionalBackground;

    private String headImg;

    private int servicePrice;

    private String introduction;

    private Integer managerId;

    private List<ConsultantFieldVo> consultantFieldVos;

}
