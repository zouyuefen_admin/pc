package cn.org.theo.vo;

import lombok.Data;

/**
 * project name: IntelliJ IDEA
 * Date: 2022/8/25 23:22
 *
 * @PackageName: cn.org.theo.vo
 * @ClassName: ScheduleVo
 * @Author: Theo
 * @Version:
 * @Description:
 */
@Data
public class ScheduleVo {

    private Integer id;
    private String slot;
    private String dateTime;
    private Integer managerId;
    private Integer enable;


}
