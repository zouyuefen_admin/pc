package cn.org.theo.vo;

import lombok.Data;

/**
 * project name: IntelliJ IDEA
 * Date: 2022/9/5 11:01
 *
 * @PackageName: cn.org.theo.vo
 * @ClassName: AccountMoneyVo
 * @Author: Theo
 * @Version:
 * @Description:
 */
@Data
public class AccountMoneyVo {

    private Integer id;
    private String createTime;
    private String matter;
    private String consName;
    private String username;
    private String relationship;
    private int revenueExpense;
    private String revenueExpenseName;
    private int money;

}
