package cn.org.theo.vo;

import lombok.Data;

/**
 * project name: IntelliJ IDEA
 * Date: 2022/8/27 10:50
 *
 * @PackageName: cn.org.theo.vo
 * @ClassName: ConsultantFieldVo
 * @Author: Theo
 * @Version:
 * @Description:
 */
@Data
public class ConsultantFieldVo {

    private int id;

    private int consId;

    private String consName;

    private int fieldId;

    private String fieldName;
}
