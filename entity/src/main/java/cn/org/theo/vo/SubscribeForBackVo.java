package cn.org.theo.vo;

import lombok.Data;

/**
 * project name: IntelliJ IDEA
 * Date: 2022/8/30 19:19
 *
 * @PackageName: cn.org.theo.vo
 * @ClassName: SubscribeForBackVo
 * @Author: Theo
 * @Version:
 * @Description:
 */
@Data
public class SubscribeForBackVo {

    private Integer id;
    private String subTime;
    private String subId;
    private String consName;
    private String fieldName;
    private String username;
    private String finishTime;
    private String subStatus;

}
