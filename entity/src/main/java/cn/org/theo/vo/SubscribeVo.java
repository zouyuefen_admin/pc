package cn.org.theo.vo;

import lombok.Data;

import java.util.Date;

/**
 * project name: IntelliJ IDEA
 * Date: 2022/8/26 10:40
 *
 * @PackageName: cn.org.theo.qo
 * @ClassName: SubscribeQo
 * @Author: Theo
 * @Version:
 * @Description:
 */
@Data
public class SubscribeVo {


    private int id;
    //
    private String uUsername;
    //
    private String mUsername;

    private String subStatus;

    private String questionInfo;

    private String subTime;

    private String answerTime;

    private String subId;

    //
    private String  fieldName;

    private String answer;
}
