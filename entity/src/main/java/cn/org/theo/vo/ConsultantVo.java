package cn.org.theo.vo;

import lombok.Data;

/**
 * project name: IntelliJ IDEA
 * Date: 2022/8/24 22:01
 *
 * @PackageName: cn.org.theo.vo
 * @ClassName: ConsultantVo
 * @Author: Theo
 * @Version:
 * @Description:
 */
@Data
public class ConsultantVo {
    private int id;

    private String name;

    private String school;

    private String positionalTitles;

    private Integer status;
}
