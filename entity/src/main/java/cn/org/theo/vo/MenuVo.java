package cn.org.theo.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * project name: IntelliJ IDEA
 * Date: 2022/8/24 0:13
 *
 * @PackageName: cn.org.theo.vo
 * @ClassName: MenuVo
 * @Author: Theo
 * @Version:
 * @Description:
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class MenuVo {

    private Integer id;
    private String menuName;
    private String parentName;
    private String href;
    private Integer mid;
    private Integer isParent;
    private Integer status;

}
