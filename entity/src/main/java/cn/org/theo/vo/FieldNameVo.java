package cn.org.theo.vo;

import lombok.Data;

/**
 * project name: IntelliJ IDEA
 * Date: 2022/8/28 12:59
 *
 * @PackageName: cn.org.theo.vo
 * @ClassName: FieldNameVo
 * @Author: Theo
 * @Version:
 * @Description:
 */
@Data
public class FieldNameVo {

    private String fieldName;

}
