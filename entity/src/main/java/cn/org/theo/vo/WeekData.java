package cn.org.theo.vo;

import lombok.Data;

/**
 * project name: IntelliJ IDEA
 * Date: 2022/9/8 18:36
 *
 * @PackageName: cn.org.theo.vo
 * @ClassName: WeekData
 * @Author: Theo
 * @Version:
 * @Description:
 */
@Data
public class WeekData {
    private String days;
    private Integer count;
}
