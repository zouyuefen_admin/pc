package cn.org.theo.vo;

import lombok.Data;

/**
 * project name: IntelliJ IDEA
 * Date: 2022/8/31 14:17
 *
 * @PackageName: cn.org.theo.vo
 * @ClassName: SubjectVo
 * @Author: Theo
 * @Version:
 * @Description:
 */
@Data
public class SubjectVo {

    private int id;

    private String name;
    private String option1;
    private String option2;
    private String option3;
    private String option4;

    private Integer fieldId;
    private Integer colNum;
    private String fieldName;
}
