package cn.org.theo.qo;

import lombok.Data;

/**
 * project name: IntelliJ IDEA
 * Date: 2022/8/29 13:51
 *
 * @PackageName: cn.org.theo.qo
 * @ClassName: SubscribeFrontQo
 * @Author: Theo
 * @Version:
 * @Description:
 */
@Data
public class SubscribeFrontQo {

    private Integer consId;
    private String username;
    private String fieldName;
    private String dataTime;
    private String question;
}
