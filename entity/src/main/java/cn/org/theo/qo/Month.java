package cn.org.theo.qo;

import lombok.Data;

/**
 * project name: IntelliJ IDEA
 * Date: 2022/9/8 20:24
 *
 * @PackageName: cn.org.theo.qo
 * @ClassName: Month
 * @Author: Theo
 * @Version:
 * @Description:
 */
@Data
public class Month {
    private Integer m1;
    private Integer m2;
    private Integer m3;
    private Integer m4;
    private Integer m5;
}
