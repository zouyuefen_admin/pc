package cn.org.theo.qo;

import lombok.Data;

/**
 * project name: IntelliJ IDEA
 * Date: 2022/9/8 18:12
 *
 * @PackageName: cn.org.theo.qo
 * @ClassName: Week
 * @Author: Theo
 * @Version:
 * @Description:
 */
@Data
public class Week {

    private String w1;
    private String w2;
    private String w3;
    private String w4;
    private String w5;
    private String w6;
    private String w7;

}
