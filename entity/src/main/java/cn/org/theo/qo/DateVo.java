package cn.org.theo.qo;

import lombok.Data;

/**
 * project name: IntelliJ IDEA
 * Date: 2022/9/8 22:31
 *
 * @PackageName: cn.org.theo.qo
 * @ClassName: DateVo
 * @Author: Theo
 * @Version:
 * @Description:
 */
@Data
public class DateVo {

    private String startTime;
    private String endTime;

}
