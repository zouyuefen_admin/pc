package cn.org.theo.qo;

import lombok.Data;

/**
 * project name: IntelliJ IDEA
 * Date: 2022/8/30 18:52
 *
 * @PackageName: cn.org.theo.qo
 * @ClassName: SubscribeForBackQo
 * @Author: Theo
 * @Version:
 * @Description:
 */
@Data
public class SubscribeForBackQo {

    private String username;
    private String consName;
    private String stime;
    private String etime;
}
