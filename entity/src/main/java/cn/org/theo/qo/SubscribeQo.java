package cn.org.theo.qo;

import lombok.Data;

/**
 * project name: IntelliJ IDEA
 * Date: 2022/8/26 10:40
 *
 * @PackageName: cn.org.theo.qo
 * @ClassName: SubscribeQo
 * @Author: Theo
 * @Version:
 * @Description:
 */
@Data
public class SubscribeQo {
    private String subTime;
    private String answerTime;
    private Integer subStatus;
    private String consName;
    private Integer managerId;
}
