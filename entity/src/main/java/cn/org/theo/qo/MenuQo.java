package cn.org.theo.qo;

import lombok.Data;

/**
 * project name: IntelliJ IDEA
 * Date: 2022/9/5 17:01
 *
 * @PackageName: cn.org.theo.vo
 * @ClassName: MenuQo
 * @Author: Theo
 * @Version:
 * @Description:
 */
@Data
public class MenuQo {

    private Integer pageNo;
    private Integer id;
    private String parentName;
    private String menuName;
    private String url;
}
