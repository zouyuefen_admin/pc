package cn.org.theo.qo;

import lombok.Data;

/**
 * project name: IntelliJ IDEA
 * Date: 2022/9/1 16:24
 *
 * @PackageName: cn.org.theo.qo
 * @ClassName: AssessmentQo
 * @Author: Theo
 * @Version:
 * @Description:
 */
@Data
public class AssessmentQo {
    private Integer score1;
    private Integer score2;
    private String date1;
    private String date2;
}
