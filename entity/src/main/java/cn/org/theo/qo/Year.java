package cn.org.theo.qo;

import lombok.Data;

/**
 * project name: IntelliJ IDEA
 * Date: 2022/9/8 21:25
 *
 * @PackageName: cn.org.theo.qo
 * @ClassName: Year
 * @Author: Theo
 * @Version:
 * @Description:
 */
@Data
public class Year {

    private Integer y1;
    private Integer y2;
    private Integer y3;
    private Integer y4;
    private Integer y5;
    private Integer y6;

}
