package cn.org.theo.qo;

import lombok.Data;

/**
 * project name: IntelliJ IDEA
 * Date: 2022/8/29 17:48
 *
 * @PackageName: cn.org.theo.qo
 * @ClassName: OrderListQo
 * @Author: Theo
 * @Version:
 * @Description:
 */
@Data
public class OrderListQo {

    private Integer pageNo;
    private String username;
    private String fieldName;
    private String consName;


}
