package cn.org.theo.filter;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
/**
 * project name: IntelliJ IDEA
 * Date: 2022/8/21 16:33
 *
 * @PackageName: cn.org.theo.filter
 * @ClassName: CorsFilter
 * @Author: Theo
 * @Version:
 * @Description:
 */

/**
 * CorsFileter 功能描述：CORS过滤器
 */
@Component
public class CorsFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletResponse res = (HttpServletResponse) servletResponse;

        HttpServletRequest request=(HttpServletRequest)servletRequest;

        res.setHeader("Access-Control-Allow-Origin", request.getHeader("Origin"));

        res.setHeader("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE");

        res.setHeader("Access-Control-Max-Age", "0");

        res.setHeader("Access-Control-Allow-Headers", "Origin, No-Cache, X-Requested-With, If-Modified-Since, Pragma, Last-Modified, Cache-Control, Expires, Content-Type, X-E4M-With,userId,token,token2");

        res.setHeader("Access-Control-Allow-Credentials", "true");

        res.setHeader("XDomainRequestAllowed","1");

        filterChain.doFilter(servletRequest,servletResponse);
    }

    @Override
    public void destroy() {

    }
}
