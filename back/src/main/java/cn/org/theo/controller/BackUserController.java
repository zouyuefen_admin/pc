package cn.org.theo.controller;

import cn.org.theo.pojo.Result;
import cn.org.theo.pojo.User;
import cn.org.theo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * project name: IntelliJ IDEA
 * Date: 2022/8/24 11:17
 *
 * @PackageName: cn.org.theo.controller
 * @ClassName: BackUserController
 * @Author: Theo
 * @Version:
 * @Description:
 */
@RestController()
public class BackUserController {

    @Autowired
    private UserService userService;

    @GetMapping("/initData")
    public Result initDate(int pageNo){
        return userService.initData(pageNo);
    }

    @GetMapping("/updateStatus")
    public Result updateStatus(User user, HttpServletRequest req){
        System.out.println(user);
        String username = (String) req.getSession().getAttribute("username");
        return userService.updateStatus(user,username);
    }

    @GetMapping("/del")
    public Result del(int id, HttpServletRequest req){
        String username = (String) req.getSession().getAttribute("username");
        return userService.delete(id,username);
    }
    @GetMapping("/remake")
    public Result remake(int id, HttpServletRequest req){
        String username = (String) req.getSession().getAttribute("username");
        return userService.updatePwd(id,username);
    }
    @PostMapping("/queryForCondition")
    public Result queryForCondition(User user,int pageNo){
        System.out.println(user);
        return userService.queryForCondition(user,pageNo);
    }
}
