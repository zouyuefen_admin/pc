package cn.org.theo.controller;

import cn.org.theo.pojo.Menu;
import cn.org.theo.pojo.Result;
import cn.org.theo.qo.MenuQo;
import cn.org.theo.service.MenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * project name: IntelliJ IDEA
 * Date: 2022/9/5 16:40
 *
 * @PackageName: cn.org.theo.controller
 * @ClassName: MenuController
 * @Author: Theo
 * @Version:
 * @Description:
 */
@RestController
public class MenuController {
    @Autowired
    private MenuService menuService;

    @GetMapping("/menu/getMenuList")
    public Result getMenuList(){
        return menuService.getMenuList();
    }
    @GetMapping("/menu/getOneMenu")
    public Result getOneMenu(int id){
        return menuService.getOneMenu(id);
    }

    @PostMapping("/menu/getMenuListForCondition")
    public Result getMenuListForCondition(MenuQo menuQo){

        return menuService.getMenuListForCondition(menuQo);
    }

    @PostMapping("/menu/add")
    public Result add(MenuQo menuQo){
        return menuService.add(menuQo);
    }
    @PostMapping("/menu/update")
    public Result update(MenuQo menuQo){
        return menuService.update(menuQo);
    }

    @GetMapping("/menu/delete")
    public Result delete(MenuQo menuQo){
        return menuService.delete(menuQo);
    }

    @GetMapping("/authority/getManagerList")
    public Result getManagerList(int authority){
        return menuService.getManagerList(authority);
    }
    @GetMapping("/authority/getManagerList2")
    public Result getManagerList2(int authority){
        return menuService.getManagerList2(authority);
    }
    @GetMapping("/authority/singleLeft")
    public Result singleLeft(Menu menu){
        return menuService.singleLeft(menu);
    }
    @GetMapping("/authority/singleRight")
    public Result singleRight(Menu menu){
        return menuService.singleRight(menu);
    }
    @GetMapping("/authority/doubleLeft")
    public Result doubleLeft(Menu menu){
        return menuService.doubleLeft(menu);
    }
    @GetMapping("/authority/doubleRight")
    public Result doubleRight(Menu menu){
        return menuService.doubleRight(menu);
    }
}
