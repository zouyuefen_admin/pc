package cn.org.theo.controller;

import cn.org.theo.pojo.Result;
import cn.org.theo.service.AccountMoneyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * project name: IntelliJ IDEA
 * Date: 2022/9/5 15:00
 *
 * @PackageName: cn.org.theo.controller
 * @ClassName: AccountMoneyController
 * @Author: Theo
 * @Version:
 * @Description:
 */
@RestController
public class AccountMoneyController {
    @Autowired
    private AccountMoneyService accountMoneyService;

    @GetMapping("/account/queryAccountForBack")
    public Result queryAccountForBack( String username,int pageNo){
        System.out.println(username);
        return accountMoneyService.queryAccountForBack(pageNo,username);
    }
}
