package cn.org.theo.controller;

import cn.org.theo.pojo.Result;
import cn.org.theo.service.ScheduleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * project name: IntelliJ IDEA
 * Date: 2022/8/25 15:37
 *
 * @PackageName: cn.org.theo.controller
 * @ClassName: ArrangeController
 * @Author: Theo
 * @Version:
 * @Description:
 */
@RestController
public class ArrangeController {

    @Autowired
    private ScheduleService scheduleService;

    @PostMapping("/cons/arrange")
    public Result arranger(HttpServletRequest request){
        String list = request.getParameter("list");
        String username = request.getParameter("username");
        System.out.println(list);
        System.out.println(username);
        return scheduleService.arrange(list,username);
    }

    @GetMapping("/cons/timeinfo")
    public Result timeInfo(String username){

        return scheduleService.getDateInfo(username);

    }

}
