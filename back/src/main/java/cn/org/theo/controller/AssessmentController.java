package cn.org.theo.controller;

import cn.org.theo.pojo.Result;
import cn.org.theo.qo.AssessmentQo;
import cn.org.theo.service.AssessmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * project name: IntelliJ IDEA
 * Date: 2022/9/1 16:16
 *
 * @PackageName: cn.org.theo.controller
 * @ClassName: AssessmentController
 * @Author: Theo
 * @Version:
 * @Description:
 */
@RestController
public class AssessmentController {

    @Autowired
    private AssessmentService assessmentService;

    @PostMapping("/assessment/queryForCondition")
    public Result queryForCondition(AssessmentQo assessmentQo,int pageNo){

        return assessmentService.queryForCondition(assessmentQo,pageNo);

    }
    @GetMapping("/assessment/assessmentForOne")
    public Result assessmentForOne(int id){

        return assessmentService.assessmentForOne(id);

    }

}
