package cn.org.theo.controller;

import cn.org.theo.pojo.Result;
import cn.org.theo.service.ConsultantService;
import cn.org.theo.service.FileService;
import cn.org.theo.service.SubjectService;
import cn.org.theo.vo.SubjectVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;

/**
 * project name: IntelliJ IDEA
 * Date: 2022/8/31 10:32
 *
 * @PackageName: cn.org.theo.controller
 * @ClassName: SubjectController
 * @Author: Theo
 * @Version:
 * @Description:
 */
@RestController
public class SubjectController {


    @Autowired
    private SubjectService subjectService;
    @Autowired
    private ConsultantService consultantService;
    @Autowired
    private FileService fileService ;

    @GetMapping("/getFieldList")
    public Result getFieldList() {
        return consultantService.getFieldList();
    }

    @GetMapping("/subject/getList")
    public Result getList(int pageNo, int fieldId) {
        return subjectService.getList(pageNo, fieldId);
    }

    @GetMapping("/subject/getOneSubject")
    public Result getOneSubject(int colNum, int fieldId) {
        return subjectService.getOneSubject(colNum, fieldId);
    }

    @PostMapping("/subject/updateOneSubject")
    public Result updateOneSubject(SubjectVo subjectVo) {
        return subjectService.updateOneSubject(subjectVo);
    }

    @PostMapping("/subject/addOneSubject")
    public Result addOneSubject(SubjectVo subjectVo) {
        return subjectService.addOneSubject(subjectVo);
    }

    @GetMapping("/subject/deleteOneSubject")
    public Result deleteOneSubject(SubjectVo subjectVo) {
        return subjectService.deleteOneSubject(subjectVo);
    }

    @PostMapping("/subject/file")
    public Result upload(MultipartFile file, HttpServletRequest request) throws IOException {
//        String path = System.getProperty("user.dir");//G:\apache-tomcat-8.5.81\apache-tomcat-8.5.81\bin\
        String path = request.getRequestURI() + "/upfile";
        System.out.println(path);
        String fileName = file.getOriginalFilename();
        File dir = new File(path , fileName);
        if (!dir.exists()) {
            dir.mkdirs();
        }
        file.transferTo(dir);

        return fileService.dealExcel(dir.getAbsolutePath());
    }
}
