package cn.org.theo.controller;

import cn.org.theo.pojo.Consultant;
import cn.org.theo.pojo.Result;
import cn.org.theo.service.ConsultantService;
import cn.org.theo.vo.ConsultantVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * project name: IntelliJ IDEA
 * Date: 2022/8/24 21:21
 *
 * @PackageName: cn.org.theo.controller
 * @ClassName: ConsultantController
 * @Author: Theo
 * @Version:
 * @Description:
 */
@RestController
public class ConsultantController {

    @Autowired
    private ConsultantService consultantService;

    @GetMapping("/cons/getpt")
    public Result getPt(){
        return consultantService.showPositionTitle();
    }

    @PostMapping("/cons/queryForCondition")
    public Result queryForCondition(ConsultantVo consultantVo,int pageNo){
        System.out.println(consultantVo);
        System.out.println(pageNo);
        return consultantService.query(consultantVo,pageNo);
    }
    @GetMapping("/cons/updateStatus")
    public Result updateStatus(ConsultantVo consultantVo, HttpServletRequest req){
        return consultantService.updateStatus(consultantVo, (String) req.getSession().getAttribute("username"));
    }

    @GetMapping("/cons/del")
    public Result del(int id, HttpServletRequest req){
        return consultantService.del(id, (String) req.getSession().getAttribute("username"));
    }
    @GetMapping("/cons/remake")
    public Result remake(int id, HttpServletRequest req){
        String username = (String) req.getSession().getAttribute("username");
        return consultantService.updatePwd(id,username);
    }
    @PostMapping("/cons/addAdmin")
    public Result addAdmin(Consultant consultant){

        return consultantService.addAdmin(consultant);
    }

}
