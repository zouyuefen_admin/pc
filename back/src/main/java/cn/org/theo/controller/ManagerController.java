package cn.org.theo.controller;


import cn.org.theo.pojo.Manager;
import cn.org.theo.pojo.Result;

import cn.org.theo.service.ManagerService;
import cn.org.theo.service.UserService;
import cn.org.theo.utils.DesUtil;
import cn.org.theo.utils.Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * project name: IntelliJ IDEA
 * Date: 2022/8/23 21:43
 *
 * @PackageName: cn.org.theo.controller
 * @ClassName: ManagerController
 * @Author: Theo
 * @Version:
 * @Description:
 */
@RestController
@CrossOrigin
public class ManagerController {

    @Autowired
    private ManagerService managerService;
    @PostMapping("/mlogin")
    public Result login(Manager manager, HttpServletRequest req) throws Exception {
        System.out.println(manager);
        int i = managerService.login(manager);
        if(i == 2){
            System.out.println(i+"aaaaaaaaaaa");
            return new Result(-1,"登陆失败，该账号已被封禁！请联系管理员 6666-66-666","");
        }
        if (i == 1){
            //token密匙
            String token = Util.getToken(manager.getUsername());
            String s = DesUtil.DES1(token);
            return Result.updateResp(i,"登录成功,token获取成功！","登录失败,用户名或密码错误！",s);
        }
        return Result.updateResp(i,"登录成功","登录失败,用户名或密码错误！",null);
    }

    @GetMapping("/getAdminInfo")
    public Result getAdminInfo(HttpServletRequest req) throws Exception {
        String token2 = req.getHeader("token2");
        System.out.println(token2+"   aaaaaaaaaaaa");
        String s = DesUtil.DES2(token2);
        System.out.println("控制器：解密后的数据：" + s);
        String username = s.substring(32);
        System.out.println("截取的数据" + username);
        return managerService.getAdminInfo(username);
    }


}
