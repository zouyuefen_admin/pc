package cn.org.theo.controller;

import cn.org.theo.pojo.Result;
import cn.org.theo.qo.DateVo;
import cn.org.theo.qo.Month;
import cn.org.theo.qo.Week;
import cn.org.theo.qo.Year;
import cn.org.theo.service.StatisticsService;
import com.sun.corba.se.spi.monitoring.StatisticsAccumulator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * project name: IntelliJ IDEA
 * Date: 2022/9/8 18:11
 *
 * @PackageName: cn.org.theo.controller
 * @ClassName: StatisticsController
 * @Author: Theo
 * @Version:
 * @Description:
 */
@RestController
public class StatisticsController {

    @Autowired
    private StatisticsService statisticsService;


    @PostMapping("/statistics/queryUserTotalForWeek")
    public Result queryUserTotalForWeek(Week week){
        System.out.println(week);
        return statisticsService.queryUserTotalForWeek(week);
    }
    @PostMapping("/statistics/queryUserTotalForMonth")
    public Result queryUserTotalForMonth(Month month){
        System.out.println(month);
        return statisticsService.queryUserTotalForMonth(month);
    }
    @PostMapping("/statistics/queryUserTotalForYear")
    public Result queryUserTotalForYear(Year year){
        System.out.println(year);
        return statisticsService.queryUserTotalForYear(year);
    }
    @PostMapping("/statistics/querySubForData")
    public Result querySubForData(DateVo dateVo){
        System.out.println(dateVo);
        return statisticsService.querySubForData(dateVo);
    }

}
