package cn.org.theo.controller;

import cn.org.theo.pojo.Result;
import cn.org.theo.pojo.Subscribe;
import cn.org.theo.qo.SubscribeForBackQo;
import cn.org.theo.service.SubscribeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * project name: IntelliJ IDEA
 * Date: 2022/8/30 18:50
 *
 * @PackageName: cn.org.theo.controller
 * @ClassName: SubscribeController
 * @Author: Theo
 * @Version:
 * @Description:
 */
@RestController
public class SubscribeController {

    @Autowired
    private SubscribeService subscribeService;

    @PostMapping("/sub/queryForCondition")
    public Result getSubList(SubscribeForBackQo subscribeForBackQo,int pageNo){
        return subscribeService.getSubList(subscribeForBackQo,pageNo);
    }
    @GetMapping("/sub/ban")
    public Result ban(int id){
        return subscribeService.ban(id);
    }
}
