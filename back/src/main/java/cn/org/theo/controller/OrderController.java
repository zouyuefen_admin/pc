package cn.org.theo.controller;

import cn.org.theo.pojo.Result;
import cn.org.theo.pojo.Subscribe;
import cn.org.theo.qo.SubscribeQo;
import cn.org.theo.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * project name: IntelliJ IDEA
 * Date: 2022/8/26 9:55
 *
 * @PackageName: cn.org.theo.controller
 * @ClassName: OrderController
 * @Author: Theo
 * @Version:
 * @Description:
 */
@RestController
public class OrderController {

    @Autowired
    private OrderService orderService;


    @GetMapping("/order/getselect")
    public Result getSelect(){
        return orderService.getOrderStatusList();
    }



    @GetMapping("/order/getList")
    public Result getList(SubscribeQo subscribeQo,int pageNo){
        return orderService.getListForPage(subscribeQo,pageNo);
    }
    @GetMapping("/order/changeConfirm")
    public Result changeConfirm(int id){
        return orderService.updateStatus(id);
    }
    @PostMapping("/order/diagnosis")
    public Result diagnosis(int id,String answer){
        return orderService.updateStatusForDiagnosis(id,answer);
    }
    @GetMapping("/order/subscribeInfoForOne")
    public Result subscribeInfoForOne(int id){
        return orderService.subscribeInfoForOne(id);
    }


}
