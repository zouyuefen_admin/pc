package cn.org.theo.utils;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import org.apache.commons.codec.binary.Base64;
/**
 * project name: IntelliJ IDEA
 * Date: 2022/9/2 14:27
 *
 * @PackageName: cn.org.theo.utils
 * @ClassName: DesUtil
 * @Author: Theo
 * @Version:
 * @Description:
 */
public class DesUtil {

    public static String DES1(String plainText) throws Exception {
        // 明文
//        String plainText = "4b83dd4e98844acc90cf609f4ea9193bTheo666";
        System.out.println("明文：" + plainText);

        // 提供原始秘钥:长度64位,8字节
        String originKey = "12345678";
        // 根据给定的字节数组构建一个秘钥
        SecretKeySpec key = new SecretKeySpec(originKey.getBytes(), "DES");

        // 加密
        // 1.获取加密算法工具类
        Cipher cipher = Cipher.getInstance("DES");
        // 2.对工具类对象进行初始化,
        // mode:加密/解密模式
        // key:对原始秘钥处理之后的秘钥
        cipher.init(Cipher.ENCRYPT_MODE, key);
        // 3.用加密工具类对象对明文进行加密
        byte[] encipherByte = cipher.doFinal(plainText.getBytes());
        // 防止乱码，使用Base64编码
        String encode = Base64.encodeBase64String(encipherByte);
        return encode;

    }
    public static String DES2(String encode) throws Exception {
        String originKey = "12345678";
        // 根据给定的字节数组构建一个秘钥
        SecretKeySpec key = new SecretKeySpec(originKey.getBytes(), "DES");
        // 加密
        // 1.获取加密算法工具类
        Cipher cipher = Cipher.getInstance("DES");
        cipher.init(Cipher.DECRYPT_MODE, key);
        // 3.用加密工具类对象对密文进行解密
        byte[] decode = Base64.decodeBase64(encode);
        byte[] decipherByte = cipher.doFinal(decode);
        String decipherText = new String(decipherByte);
        return decipherText;
    }

}
