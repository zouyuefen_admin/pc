package cn.org.theo.utils;

import org.aspectj.lang.ProceedingJoinPoint;

/**
 * project name: IntelliJ IDEA
 * Date: 2022/8/18 21:13
 *
 * @PackageName: com.theo.utils
 * @ClassName: PowerAspect
 * @Author: Theo
 * @Version:
 * @Description:
 */
public class PowerAspect {

    public void start(){
        System.out.println("前置通知！");
    }
    public void end(){
        System.out.println("后置通知！");
    }

    public Object around(ProceedingJoinPoint proceedingJoinPoint ) throws Throwable {
        System.out.println("后置!");
        Object result = null;
        //织入我们的代码
        Object[] args = proceedingJoinPoint.getArgs();
        long l = System.currentTimeMillis();
        result = proceedingJoinPoint.proceed(args);
        System.out.println("耗时：" + (System.currentTimeMillis() - l));
        return result;
    }

}
