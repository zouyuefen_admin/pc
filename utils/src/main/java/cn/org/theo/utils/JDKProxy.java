package cn.org.theo.utils;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

/**
 * project name: IntelliJ IDEA
 * Date: 2022/8/18 20:24
 *
 * @PackageName: com.theo.utils
 * @ClassName: JDKProxy
 * @Author: Theo
 * @Version:
 * @Description: 代理模式，实现aop
 */
public class JDKProxy implements InvocationHandler {

    private Object object; //原有对象

    public JDKProxy(Object object) {
        this.object = object;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        //增强方法
        long l = System.currentTimeMillis();
        //通过反射调用方法
        Object res = method.invoke(object, args);
        //增强方法
        System.out.println("耗时：" + (System.currentTimeMillis() - l));
        return res;
    }

}
