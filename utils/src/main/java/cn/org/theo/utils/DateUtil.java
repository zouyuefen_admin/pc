package cn.org.theo.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * project name: IntelliJ IDEA
 * Date: 2022/8/27 0:40
 *
 * @PackageName: cn.org.theo.utils
 * @ClassName: DateUtil
 * @Author: Theo
 * @Version:
 * @Description:
 */
public class DateUtil {
    public static String usToSdf(String us){
        DateFormat df = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy", Locale.US);
        //将已有的时间字符串转化为Date对象
        try {
            Date date = df.parse(us);// 那天是周一
            // 创建所需的格式
            df = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            String str = df.format(date);// 获得格式化后的日期字符串
            return  str;// 打印最终结果
        } catch (
                ParseException e) {
            throw new RuntimeException(e);
        }
    }


}
