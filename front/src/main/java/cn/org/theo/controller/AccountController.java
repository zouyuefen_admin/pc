package cn.org.theo.controller;

import cn.org.theo.pojo.Result;
import cn.org.theo.service.AccountService;
import cn.org.theo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * project name: IntelliJ IDEA
 * Date: 2022/9/2 16:25
 *
 * @PackageName: cn.org.theo.controller
 * @ClassName: AccountController
 * @Author: Theo
 * @Version:
 * @Description:
 */
@RestController
public class AccountController {

    @Autowired
    private AccountService accountService;


    @GetMapping("/account/recharge")
    public Result recharge(int money,String username){
        return accountService.recharge(money,username);
    }
    @GetMapping("/account/getAccountInfo")
    public Result getAccountInfo(String username){
        return accountService.getAccountInfo(username);
    }

}
