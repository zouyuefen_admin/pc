package cn.org.theo.controller;

import cn.org.theo.pojo.Result;
import cn.org.theo.service.SubjectService;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * project name: IntelliJ IDEA
 * Date: 2022/8/31 20:57
 *
 * @PackageName: cn.org.theo.controller
 * @ClassName: SubjectController
 * @Author: Theo
 * @Version:
 * @Description:
 */
@RestController
public class SubjectController {

    @Autowired
    private SubjectService subjectService;


    @GetMapping("/subject/getList")
    public Result getList(int pageNo, int fieldId){
        return subjectService.getList(pageNo,fieldId);
    }
    @PostMapping("/subject/assessment")
    public Result assessment(int score, int fieldId,String username){
        return subjectService.assessment(score,fieldId,username);
    }

}
