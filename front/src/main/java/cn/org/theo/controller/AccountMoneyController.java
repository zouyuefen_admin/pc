package cn.org.theo.controller;

import cn.org.theo.mapper.AccountMoneyMapper;
import cn.org.theo.pojo.Result;
import cn.org.theo.service.AccountMoneyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * project name: IntelliJ IDEA
 * Date: 2022/9/5 10:51
 *
 * @PackageName: cn.org.theo.controller
 * @ClassName: AccountMoneyController
 * @Author: Theo
 * @Version:
 * @Description:
 */
@RestController
public class AccountMoneyController {

    @Autowired
    private AccountMoneyService accountMoneyService;

    @GetMapping("/account/getAccountList")
    public Result getAccountList(String username,int pageNo){
        return accountMoneyService.getAccountList(username,pageNo);
    }
    @GetMapping("/account/accountInfo")
    public Result accountInfo(int id){
        return accountMoneyService.accountInfo(id);
    }

}
