package cn.org.theo.controller;

import cn.org.theo.pojo.Result;
import cn.org.theo.qo.OrderListQo;
import cn.org.theo.qo.SubscribeFrontQo;
import cn.org.theo.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * project name: IntelliJ IDEA
 * Date: 2022/8/29 13:53
 *
 * @PackageName: cn.org.theo.controller
 * @ClassName: OrderController
 * @Author: Theo
 * @Version:
 * @Description:
 */
@RestController
public class OrderController {

    @Autowired
    private OrderService orderService;

    @PostMapping("/order/subscribe")
    public Result subscribe(SubscribeFrontQo subscribeFrontQo){
        return orderService.createOrder(subscribeFrontQo);
    }

    @GetMapping("/order/orderList")
    public Result orderList(String username,int pageNo){
        return orderService.showOrder(username,pageNo);
    }
    @GetMapping("/order/getConsInfo")
    public Result getConsInfo(String consName){
        return orderService.getConsInfo(consName);
    }
    @PostMapping("/order/evaluate")
    public Result evaluate(String orderId,String evaluate){
        return orderService.evaluate(orderId,evaluate);
    }
    @GetMapping("/order/haveMoney")
    public Result haveMoney(String username,int consId){
        return orderService.haveMoney(username,consId);
    }
    @GetMapping("/order/haveMoneyForOnlineExam")
    public Result haveMoney(String username){
        return orderService.haveMoneyForOnlineExam(username);
    }

}


