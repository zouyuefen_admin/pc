package cn.org.theo.controller;

import cn.org.theo.mapper.ConsultantMapper;
import cn.org.theo.pojo.Consultant;
import cn.org.theo.pojo.Result;
import cn.org.theo.pojo.User;
import cn.org.theo.service.ConsultantService;
import cn.org.theo.service.UserService;
import cn.org.theo.utils.DesUtil;
import cn.org.theo.utils.Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpRequest;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

/**
 * project name: IntelliJ IDEA
 * Date: 2022/8/21 14:32
 *
 * @PackageName: cn.org.theo.controller
 * @ClassName: UserController
 * @Author: Theo
 * @Version:
 * @Description:
 */
@RestController
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private ConsultantService consultantService;

    @PostMapping("/reg")
    public Result reg(User user){
        System.out.println(user);
        return userService.reg(user);
    }
    @PostMapping("/login")
    public Result login(User user) throws Exception {
        int i = userService.login(user);
        if(i == 2){
            System.out.println(i+"aaaaaaaaaaa");
            return new Result(-1,"登陆失败，该账号已被封禁！请联系管理员 6666-66-666","");
        }
        if (i == 1){
            //token密匙
            String token = Util.getToken(user.getUsername());
            String s = DesUtil.DES1(token);
            return new Result(0,"登录成功，欢迎" + user.getUsername(),s);
        }
        return new Result(-1,"登录失败,用户名或密码错误！" + user.getUsername(),"");
    }

    @GetMapping("/getUserInfo")
    public Result getUserInfo(HttpServletRequest req) throws Exception {
        //通过前端传的token值，从tokenMap中获取对象

        String token = req.getHeader("token");
        System.out.println(token+"   aaaaaaaaaaaa");
        String s = DesUtil.DES2(token);
        System.out.println("控制器：解密后的数据：" + s);
        String username = s.substring(32);
        System.out.println("截取的数据" + username);
        return Result.updateResp(token == null ? 0:1,"获取成功","获取失败",username);
    }
    @PostMapping("/getConsInfo")
    public Result getConsInfo(HttpServletRequest req){
        String consName = req.getParameter("consName");
        String fieldNameList = req.getParameter("fieldName");
        Integer pageNo = Integer.valueOf(req.getParameter("pageNo"));
        System.out.println(fieldNameList);
        System.out.println(consName);
        System.out.println(pageNo);
        return consultantService.queryByFront(fieldNameList,consName,pageNo);
    }
    @GetMapping("/getConsInfoByIndex")
    public Result getConsInfoByIndex(){
        return consultantService.getConsInfoByIndex();
    }
    @GetMapping("/getFieldList")
    public Result getFieldList(){
        return consultantService.getFieldList();
    }
    @GetMapping("/timeInfo")
    public Result timeInfo(int consId){
        return consultantService.timeInfo(consId);
    }


}
